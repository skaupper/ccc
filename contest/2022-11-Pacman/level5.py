from typing import Iterable, Iterator, List, Sequence, Generator, Set
from dataclasses import dataclass, field
import algorithms.graphs as graphs
import copy


@dataclass(frozen=True, eq=True)
class Point:
    x: int
    y: int
    step_idx: int = 0

    def get_distance(self, other: "Point") -> int:
        return abs(self.x - other.x) + abs(self.y - other.y)


@dataclass
class Entity:
    x: int
    y: int
    moves: List[str] = field(default_factory=list)
    coin_count: int = 0
    is_player: bool = False
    is_alive: bool = True
    _next_move: int = 0

    @classmethod
    def from_str(
        cls,
        lines: Iterator[str],
        is_player: bool = False,
        parse_movement: bool = True,
    ) -> "Entity":
        y, x = [int(coord) for coord in next(lines).split(' ')]

        if parse_movement:
            next(lines)  # nr of moves
            moves = [c for c in next(lines)]
        else:
            moves = []
        return cls(x - 1, y - 1, moves, is_player=is_player)

    def move(self) -> None:
        m = self.moves[self._next_move]
        self._next_move = (self._next_move + 1) % len(self.moves)

        if m == 'U':
            self.y -= 1
        elif m == 'D':
            self.y += 1
        elif m == 'R':
            self.x += 1
        elif m == 'L':
            self.x -= 1

    def collides(self, other: "Entity") -> bool:
        return self.x == other.x and self.y == other.y

    def get_pos(self) -> Point:
        return Point(self.x, self.y)


def mirror_moves(moves: List[str]) -> Generator[str, None, None]:
    for m in moves:
        if m == "U":
            yield "D"
        elif m == "D":
            yield "U"
        elif m == "R":
            yield "L"
        elif m == "L":
            yield "R"


def get_all_positions(ghost: Entity) -> Generator[Point, None, None]:
    curr_pos = ghost.get_pos()
    yield curr_pos
    x = curr_pos.x
    y = curr_pos.y

    step_idx = 0

    for m in [*ghost.moves, *list(mirror_moves(ghost.moves))[:-1]]:
        step_idx += 1
        if m == 'U':
            y -= 1
        elif m == 'D':
            y += 1
        elif m == 'R':
            x += 1
        elif m == 'L':
            x -= 1
        yield Point(x, y, step_idx)


class BoardPathProtocol:

    def __init__(
        self,
        board: Sequence[Sequence[str]],
        coin: Point,
        ghost_positions: Sequence[Sequence[Point]],
    ):
        self.board = board
        self.coin = coin
        self.ghost_positions = ghost_positions

    def get_neighbours(self, point: Point) -> List[graphs.Edge[Point]]:
        next_move = (point.step_idx + 1) % len(self.ghost_positions)

        edges: List[graphs.Edge[Point]] = []
        potential_neighbours = [
            Point(point.x - 1, point.y, next_move),
            Point(point.x + 1, point.y, next_move),
            Point(point.x, point.y - 1, next_move),
            Point(point.x, point.y + 1, next_move),
        ]

        for p in potential_neighbours:
            collides_with_ghost = p in self.ghost_positions[next_move]
            if self.board[p.y][p.x] == "W" or collides_with_ghost:
                continue

            if p.x < 0 or p.x >= len(self.board[0]):
                continue
            if p.y < 0 or p.y >= len(self.board):
                continue

            edges.append(graphs.Edge(p))

        return edges

    def get_distance_to_target(self, point: Point) -> float:
        return point.get_distance(self.coin)

    def is_target(self, point: Point) -> bool:
        return point.x == self.coin.x and point.y == self.coin.y


def stringify(points: Sequence[Point]) -> str:
    directions: List[str] = []

    for i in range(1, len(points)):
        diffx = points[i].x - points[i - 1].x
        diffy = points[i].y - points[i - 1].y
        if diffx == 1:
            directions.append("R")
        elif diffx == -1:
            directions.append("L")
        elif diffy == 1:
            directions.append("D")
        elif diffy == -1:
            directions.append("U")
        assert abs(diffx) + abs(diffy) == 1

    return "".join(directions)


def simulate(
    board: List[List[str]],
    pacman: Entity,
    ghosts: Sequence[Entity],
    moves: str,
):
    pacman.moves = [c for c in moves]
    for _ in range(len(moves)):
        pacman.move()
        for g in ghosts:
            g.move()

        player_cell = board[pacman.y][pacman.x]
        is_dead = (player_cell == "W"
                   or any(pacman.collides(g) for g in ghosts))
        if is_dead:
            pacman.is_alive = False
            break
        if player_cell == "C":
            pacman.coin_count += 1
            board[pacman.y][pacman.x] = " "

    assert pacman.coin_count == 1 and pacman.is_alive


def run(lines: Iterator[str]) -> Iterable[str]:
    board_lines = int(next(lines))
    board = [[c if c not in ["P", "E", "G"] else " " for c in next(lines)]
             for _ in range(board_lines)]

    pacman = Entity.from_str(lines, True, parse_movement=False)
    ghost_count = int(next(lines))
    ghosts: List[Entity] = []
    for _ in range(ghost_count):
        ghosts.append(Entity.from_str(lines, parse_movement=True))

    max_moves = int(next(lines))

    coin_positions: Set[Point] = set([])
    for y, row in enumerate(board):
        for x, cell in enumerate(row):
            if cell == "C":
                coin_positions.add(Point(x, y))
    assert len(coin_positions) == 1
    coin = next(iter(coin_positions))

    ghost_positions = list(zip(*[list(get_all_positions(g)) for g in ghosts]))

    backup_board = copy.deepcopy(board)
    backup_pacman = copy.deepcopy(pacman)
    backup_ghosts = copy.deepcopy(ghosts)

    path = graphs.get_shortest_path(
        pacman.get_pos(),
        BoardPathProtocol(board, coin, ghost_positions),
    )
    assert path is not None
    assert len(path) < max_moves
    str_path = stringify(path)

    simulate(
        backup_board,
        backup_pacman,
        backup_ghosts,
        str_path,
    )
    return [str_path]
