from typing import Iterable, Iterator, List, Sequence
from dataclasses import dataclass


@dataclass
class Entity:
    x: int
    y: int
    moves: List[str]
    coin_count: int = 0
    is_player: bool = False
    is_alive: bool = True
    _next_move: int = 0

    @classmethod
    def from_str(cls,
                 lines: Iterator[str],
                 is_player: bool = False) -> "Entity":
        y, x = [int(coord) for coord in next(lines).split(' ')]
        next(lines)  # nr of moves
        moves = next(lines)
        return cls(x - 1, y - 1, [c for c in moves], is_player=is_player)

    def move(self) -> None:
        m = self.moves[self._next_move]
        self._next_move = (self._next_move + 1) % len(self.moves)

        if m == 'U':
            self.y -= 1
        elif m == 'D':
            self.y += 1
        elif m == 'R':
            self.x += 1
        elif m == 'L':
            self.x -= 1

    def collides(self, other: "Entity") -> bool:
        return self.x == other.x and self.y == other.y


def tick(board: List[List[str]], player: Entity,
         ghosts: Sequence[Entity]) -> bool:

    print(player.moves[player._next_move])

    player.move()
    for g in ghosts:
        g.move()

    player_cell = board[player.y][player.x]

    is_dead = (player_cell == "W" or any(player.collides(g) for g in ghosts))
    if is_dead:
        player.is_alive = False
        return False

    if player_cell == "C":
        player.coin_count += 1
        board[player.y][player.x] = " "

    if player._next_move == 0:
        return False

    return True


def print_board(board: Sequence[Sequence[str]]):
    for row in board:
        print("".join(row))
    print()


def run(lines: Iterator[str]) -> Iterable[str]:
    board_lines = int(next(lines))
    board = [[c if c not in ["P", "G"] else " " for c in next(lines)]
             for _ in range(board_lines)]
    print_board(board)

    pacman = Entity.from_str(lines, True)
    ghost_count = int(next(lines))
    ghosts: List[Entity] = []
    for _ in range(ghost_count):
        ghosts.append(Entity.from_str(lines))

    while tick(board, pacman, ghosts):
        pass

    return [f"{pacman.coin_count} {'YES' if pacman.is_alive else 'NO'}"]
