from typing import Iterable, Iterator


def run(lines: Iterator[str]) -> Iterable[str]:
    board_lines = int(next(lines))
    board = [[c for c in next(lines)] for _ in range(board_lines)]

    p_y, p_x = [int(coord) for coord in next(lines).split(' ')]
    p_y -= 1
    p_x -= 1
    next(lines)
    moves = next(lines)

    coin_count = 0

    for m in moves:
        if m == 'U':
            p_y -= 1
        elif m == 'D':
            p_y += 1
        elif m == 'R':
            p_x += 1
        elif m == 'L':
            p_x -= 1

        if board[p_y][p_x] == 'C':
            coin_count += 1
            board[p_y][p_x] = 'E'

    return [str(coin_count)]
