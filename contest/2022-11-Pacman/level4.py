from typing import Iterable, Iterator, List, Sequence, Generator, Optional, Set, Tuple
from dataclasses import dataclass, field


@dataclass(frozen=True, eq=True)
class Point:
    x: int
    y: int


@dataclass
class Entity:
    x: int
    y: int
    moves: List[str] = field(default_factory=list)
    coin_count: int = 0
    is_player: bool = False
    is_alive: bool = True
    _next_move: int = 0

    @classmethod
    def from_str(cls,
                 lines: Iterator[str],
                 is_player: bool = False,
                 parse_movement: bool = True) -> "Entity":
        y, x = [int(coord) for coord in next(lines).split(' ')]

        if parse_movement:
            next(lines)  # nr of moves
            moves = [c for c in next(lines)]
        else:
            moves = []
        return cls(x - 1, y - 1, moves, is_player=is_player)

    def move(self) -> None:
        m = self.moves[self._next_move]
        self._next_move = (self._next_move + 1) % len(self.moves)

        if m == 'U':
            self.y -= 1
        elif m == 'D':
            self.y += 1
        elif m == 'R':
            self.x += 1
        elif m == 'L':
            self.x -= 1

    def collides(self, other: "Entity") -> bool:
        return self.x == other.x and self.y == other.y

    def get_pos(self) -> Point:
        return Point(self.x, self.y)


def tick(board: List[List[str]], player: Entity,
         ghosts: Sequence[Entity]) -> bool:

    print(player.moves[player._next_move])

    player.move()
    for g in ghosts:
        g.move()

    player_cell = board[player.y][player.x]

    is_dead = (player_cell == "W" or any(player.collides(g) for g in ghosts))
    if is_dead:
        player.is_alive = False
        return False

    if player_cell == "C":
        player.coin_count += 1
        board[player.y][player.x] = " "

    if player._next_move == 0:
        return False

    return True


def print_board(board: Sequence[Sequence[str]]):
    for row in board:
        print("".join(row))
    print()


def get_neighbours(board: Sequence[Sequence[str]],
                   pos: Point) -> Generator[Point, None, None]:
    if pos.x > 0 and board[pos.y][pos.x - 1] not in ["W", "G"]:
        yield Point(pos.x - 1, pos.y)
    if pos.x < len(board[0]) - 1 and board[pos.y][pos.x + 1] not in ["W", "G"]:
        yield Point(pos.x + 1, pos.y)
    if pos.y > 0 and board[pos.y - 1][pos.x] not in ["W", "G"]:
        yield Point(pos.x, pos.y - 1)
    if pos.y < len(board) - 1 and board[pos.y + 1][pos.x] not in ["W", "G"]:
        yield Point(pos.x, pos.y + 1)


def get_path(
    board,
    player_point,
    coin,
) -> Optional[List[Point]]:
    visited: Set[Point] = set([])
    to_visit: List[Tuple[Point, List[Point]]] = [(player_point, [])]

    while len(to_visit) > 0:
        p, path = to_visit[0]
        to_visit.remove((p, path))

        visited.add(p)
        if p == coin:
            return path

        neighbours = [n for n in get_neighbours(board, p) if n not in visited]
        for n in neighbours:
            to_visit.append((n, [*path, n]))

    return None


def apply_path(board: List[List[str]], player: Entity, path: List[Point],
               coin_positions: Set[Point]) -> None:
    for p in path:
        player.x = p.x
        player.y = p.y
        if board[p.y][p.x] == "C":
            player.coin_count += 1
            coin_positions.remove(p)
        board[p.y][p.x] = " "


def stringify(points: Sequence[Point]) -> str:
    directions: List[str] = []

    for i in range(1, len(points)):
        diffx = points[i].x - points[i - 1].x
        diffy = points[i].y - points[i - 1].y
        if diffx == 1:
            directions.append("R")
        elif diffx == -1:
            directions.append("L")
        elif diffy == 1:
            directions.append("D")
        elif diffy == -1:
            directions.append("U")
        assert abs(diffx) + abs(diffy) == 1

    return "".join(directions)


def run(lines: Iterator[str]) -> Iterable[str]:
    board_lines = int(next(lines))
    board = [[c if c not in ["P"] else " " for c in next(lines)]
             for _ in range(board_lines)]

    pacman = Entity.from_str(lines, True, parse_movement=False)
    ghosts: List[Entity] = []
    coin_positions = set([])
    for y, row in enumerate(board):
        for x, cell in enumerate(row):
            if cell == "G":
                ghosts.append(Entity(x=x, y=y))
            elif cell == "C":
                coin_positions.add(Point(x, y))

    # max_moves = int(next(lines))

    orig_pos = pacman.get_pos()
    total_path: List[Point] = []
    while len(coin_positions) > 0:
        coin = min(coin_positions,
                   key=lambda c: abs(c.x - pacman.x) + abs(c.y - pacman.y))
        path = get_path(board, pacman.get_pos(), coin)
        assert path is not None
        apply_path(board, pacman, path, coin_positions)
        total_path += path

    return [stringify([orig_pos] + total_path)]
