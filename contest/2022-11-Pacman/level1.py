from typing import Iterable, Iterator


def run(lines: Iterator[str]) -> Iterable[str]:
    board_lines = int(next(lines))
    board = [next(lines) for _ in range(board_lines)]

    return [str(sum([row.count('C') for row in board]))]
