from typing import AbstractSet, Iterable, Iterator, Mapping, List
from enum import Enum
from dataclasses import dataclass
from graphs import *

@dataclass(frozen=True)
class Point:
    x: int
    y: int

    @classmethod
    def from_str(cls, s):
        x, y = map(int, s.split(','))
        return cls(x=x, y=y)

    def to_str(self) -> str:
        return f"{self.x},{self.y}"

class Tile(Enum):
    Land = 0,
    Water = 1

    @classmethod
    def from_str(cls, s):
        if (s == "L"):
            return cls.Land
        elif (s == "W"):
            return cls.Water
        assert False

    def __str__(self) -> str:
        if (self == self.Land):
            return "L"
        elif (self == self.Water):
            return "W"
        assert False


def get_neighbours(point: Point, tile_map: Mapping[Point, Tile]) -> List[Point]:
    candidates = [
        Point(point.x - 1, point.y), # left
        Point(point.x, point.y - 1), # top
        Point(point.x + 1, point.y), # right
        Point(point.x, point.y + 1), # bottom
        Point(point.x - 1, point.y - 1), # left top
        Point(point.x + 1, point.y - 1), # right top
        Point(point.x + 1, point.y + 1), # right bottom
        Point(point.x - 1, point.y + 1) # left bottom
    ]
    return candidates

@dataclass
class PathFinderImpl:
    tile_map: Dict[Point, Tile]
    map_size: int
    end: Point
    origin: Point
    drive_below_island: bool

    def get_neighbours(self, point: Point) -> List[Edge[T]]:
        candidates = get_neighbours(point, self.tile_map)

        filtered_candidates = []
        for c in candidates:
            if c.x < 0 or c.x >= self.map_size:
                continue

            if c.y < 0 or c.y >= self.map_size:
                continue

            if self.tile_map[c] == Tile.Land:
                continue

            if c.x == self.origin.x:
                delta_y = c.y - self.origin.y
                if self.drive_below_island and delta_y < 0:
                    continue
                if not self.drive_below_island and delta_y > 0:
                    continue

            filtered_candidates.append(c)
        
        # if point.x == 21 and point.y == 2:
        #     print(f"p: {point}")
        #     print(f"filtered c: {filtered_candidates}")

        # print(f"p: {point}")
        # print(f"filtered c: {filtered_candidates}")
        # input()

        edges = [Edge(dest=p) for p in filtered_candidates]
        return edges

    def get_distance_to_target(self, point: Point) -> float:
        return abs(self.end.x - point.x) + abs(self.end.y - point.y)

    def is_target(self, point: Point) -> bool:
        return point == self.end

def get_island_points(p: Point, tile_map: Mapping[Point, Tile]) -> AbstractSet[Point]:
    visited = set([])
    to_visit = [p]

    while len(to_visit) > 0:
        next_visit = to_visit.pop()
        visited.add(next_visit)     
        left = Point(next_visit.x - 1, next_visit.y)
        top = Point(next_visit.x, next_visit.y - 1)
        right = Point(next_visit.x + 1, next_visit.y)
        bottom = Point(next_visit.x, next_visit.y + 1)
        neighbours = [left, top, right, bottom]
        
        for neighbour in neighbours:
            if neighbour not in tile_map:
                continue
        
            if neighbour in visited:
                continue

            if tile_map[neighbour] != Tile.Land:
                continue

            to_visit.append(neighbour)

    return visited

def does_path_intersect(route_points: List[Point]) -> bool:
    visited = set([])
    last_point = None
    predecessors = {}

    # print("-- new route")

    for p in route_points:
        if p in visited:
            # print("p visited: " + p.to_str())
            return True
        
        visited.add(p)

        if last_point is None:
            last_point = p
            continue
        delta_x = p.x - last_point.x
        delta_y = p.y - last_point.y
        if delta_x == 0 or delta_y == 0:
            last_point = p
            continue

        assert abs(delta_x) <= 1
        assert abs(delta_y) <= 1, delta_y
        assert abs(delta_x) + abs(delta_y) <= 2

        rotated_point1 = Point(p.x, last_point.y)
        rotated_point2 = Point(last_point.x, p.y)

        # print(rotated_point1)
        # print(rotated_point2)

        if rotated_point1 in predecessors:
            if predecessors[rotated_point1] == rotated_point2:
                # print("digonally 1: " + p.to_str())
                return True

        if rotated_point2 in predecessors:
            if predecessors[rotated_point2] == rotated_point1:
                # print("digonally 2: " + p.to_str())
                return True

        if last_point is not None:
            predecessors[p] = last_point

        last_point = p

    return False

def get_circumference(origin: Point, tile_map: Mapping[Point, Tile], map_size: int) -> List[Point]:
    island_points = get_island_points(origin, tile_map)
    island_sorted_x = list(sorted(list(island_points), key=lambda p: p.x))
    start = Point(island_sorted_x[0].x - 1, island_sorted_x[0].y)
    end = Point(island_sorted_x[-1].x + 1, island_sorted_x[-1].y)
    # print(origin, start, end)

    path_finder_above = PathFinderImpl(tile_map=tile_map, map_size=map_size, end=end, origin=origin, drive_below_island=False)
    path_finder_below = PathFinderImpl(tile_map=tile_map, map_size=map_size, end=end, origin=origin, drive_below_island=True)

    path_above = get_shortest_path(start, path_finder_above)
    path_below = get_shortest_path(start, path_finder_below)

    for i in range(len(path_above)):
        if path_above[i] != path_below[i]:
            trim_front = i - 1
            break
    else:
        assert False

    for i in range(len(path_above)):
        if path_above[-i-1] != path_below[-i-1]:
            trim_back = i - 1
            break
    else:
        assert False

    # print(f"trim_front: {trim_front}")
    # print(f"trim_back: {trim_back}")

    path_above = path_above[trim_front:len(path_above) - trim_back]
    path_below = path_below[trim_front:len(path_below) - trim_back]
    
    # print("above: " + " ".join(map(Point.to_str, path_above)))
    # print("below: " + " ".join(map(Point.to_str, path_below)))

    full_path = path_above + list(reversed(path_below[1:-1]))
    # print(len(full_path))
    # print("-------------------")
    # print(" ".join(map(Point.to_str, full_path)))

    assert full_path is not None
    assert len(full_path) > 0
    assert len(full_path) <= 2 * map_size
    assert not does_path_intersect(full_path)
    return full_path

    # delete duplicates
    # path_below = 
    # return []

def run(lines: Iterator[str]) -> Iterable[str]:
    tile_map = {}
    size = int(next(lines))
    for y in range(size):
        line = next(lines)
        for x in range(size):
            char = line[x]
            p = Point(x, y)
            tile_map[p] = Tile.from_str(char)

    number_pairs = int(next(lines))
    for i in range(number_pairs):
        origin = Point.from_str(next(lines))
        yield " ".join(map(Point.to_str, get_circumference(origin, tile_map, size)))
