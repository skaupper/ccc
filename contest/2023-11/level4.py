from typing import Iterable, Iterator, Mapping, List
from enum import Enum
from dataclasses import dataclass
from graphs import *

@dataclass(frozen=True)
class Point:
    x: int
    y: int

    @classmethod
    def from_str(cls, s):
        x, y = map(int, s.split(','))
        return cls(x=x, y=y)

    def to_str(self) -> str:
        return f"{self.x},{self.y}"

class Tile(Enum):
    Land = 0,
    Water = 1

    @classmethod
    def from_str(cls, s):
        if (s == "L"):
            return cls.Land
        elif (s == "W"):
            return cls.Water
        assert False

    def __str__(self) -> str:
        if (self == self.Land):
            return "L"
        elif (self == self.Water):
            return "W"
        assert False

@dataclass
class PathFinderImpl:
    tile_map: Dict[Point, Tile]
    map_size: int
    start: Point
    end: Point

    def get_neighbours(self, point: Point) -> List[Edge[T]]:
        candidates = [
            Point(point.x - 1, point.y), # left
            Point(point.x, point.y - 1), # top
            Point(point.x + 1, point.y), # right
            Point(point.x, point.y + 1), # bottom
            Point(point.x - 1, point.y - 1), # left top
            Point(point.x + 1, point.y - 1), # right top
            Point(point.x + 1, point.y + 1), # right bottom
            Point(point.x - 1, point.y + 1) # left bottom
        ]

        filtered_candidates = [p for p in candidates if
            p.x >= 0 and p.x < self.map_size and
            p.y >= 0 and p.y < self.map_size and
            self.tile_map[p] == Tile.Water]

        edges = [Edge(dest=p) for p in filtered_candidates]
        return edges
        

    def get_distance_to_target(self, point: Point) -> float:
        return abs(self.end.y - point.x) + abs(self.end.y - point.y)

    def is_target(self, point: Point) -> bool:
        return point == self.end

def are_on_same_island(p1, p2: Point, tile_map: Mapping[Point, Tile]) -> bool:
    visited = set([])
    to_visit = [p1]

    while len(to_visit) > 0:
        next_visit = to_visit.pop()
        visited.add(next_visit)     
        left = Point(next_visit.x - 1, next_visit.y)
        top = Point(next_visit.x, next_visit.y - 1)
        right = Point(next_visit.x + 1, next_visit.y)
        bottom = Point(next_visit.x, next_visit.y + 1)
        neighbours = [left, top, right, bottom]
        
        for neighbour in neighbours:
            if neighbour not in tile_map:
                continue
        
            if neighbour in visited:
                continue

            if tile_map[neighbour] != Tile.Land:
                continue

            to_visit.append(neighbour)

    return p2 in visited

def does_path_intersect(route_points: List[Point], ) -> bool:
    visited = set([])
    last_point = None
    predecessors = {}

    # print("-- new route")

    for p in route_points:
        if p in visited:
            return True
        
        visited.add(p)

        if last_point is None:
            last_point = p
            continue
        delta_x = p.x - last_point.x
        delta_y = p.y - last_point.y
        if delta_x == 0 or delta_y == 0:
            last_point = p
            continue

        assert abs(delta_x) <= 1
        assert abs(delta_y) <= 1, delta_y
        assert abs(delta_x) + abs(delta_y) <= 2

        rotated_point1 = Point(p.x, last_point.y)
        rotated_point2 = Point(last_point.x, p.y)

        # print(rotated_point1)
        # print(rotated_point2)

        if rotated_point1 in predecessors:
            if predecessors[rotated_point1] == rotated_point2:
                return True

        if rotated_point2 in predecessors:
            if predecessors[rotated_point2] == rotated_point1:
                return True

        if last_point is not None:
            predecessors[p] = last_point

        last_point = p

    return False

def get_path(start, end: Point, tile_map: Mapping[Point, Tile], map_size: int):
    path_finder = PathFinderImpl(tile_map, map_size, start, end)
    path = get_shortest_path(start, path_finder)

    assert path is not None
    assert len(path) > 0
    assert len(path) <= 2 * map_size
    return path


def run(lines: Iterator[str]) -> Iterable[str]:
    tile_map = {}
    size = int(next(lines))
    for y in range(size):
        line = next(lines)
        for x in range(size):
            char = line[x]
            p = Point(x, y)
            tile_map[p] = Tile.from_str(char)

    number_pairs = int(next(lines))
    for i in range(number_pairs):
        start, end = map(Point.from_str, next(lines).split(' '))
        yield " ".join(map(Point.to_str, get_path(start, end, tile_map, size)))
