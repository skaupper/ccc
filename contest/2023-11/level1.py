from typing import Iterable, Iterator
from enum import Enum

class Tile(Enum):
    Land = 0,
    Water = 1

    @classmethod
    def from_str(cls, s):
        if (s == "L"):
            return cls.Land
        elif (s == "W"):
            return cls.Water
        assert False

    def __str__(self) -> str:
        if (self == self.Land):
            return "L"
        elif (self == self.Water):
            return "W"
        assert False

def run(lines: Iterator[str]) -> Iterable[str]:
    tile_map = {}
    size = int(next(lines))
    for y in range(size):
        line = next(lines)
        for x in range(size):
            char = line[x]
            tile_map[(x, y)] = Tile.from_str(char)

    number_coords = int(next(lines))
    for i in range(number_coords):
        x, y = map(int, next(lines).split(','))
        yield tile_map[(x, y)]

