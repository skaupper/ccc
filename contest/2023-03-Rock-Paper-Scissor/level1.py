from typing import Iterable, Iterator
from enum import Enum


class Styles(Enum):
    ROCK = "R"
    PAPER = "P"
    SCISSOR = "S"

    @classmethod
    def from_str(cls, s: str) -> "Styles":
        s = s.lower()
        if s == "r":
            return cls.ROCK
        elif s == "p":
            return cls.PAPER
        elif s == "s":
            return cls.SCISSOR

        print(s)
        assert False

    def is_winning(self, other: "Styles") -> bool:
        if self == self.ROCK:
            return other == self.SCISSOR
        if self == self.PAPER:
            return other == self.ROCK
        if self == self.SCISSOR:
            return other == self.PAPER

        assert False


def run(lines: Iterator[str]) -> Iterable[str]:
    nr_of_fights = int(next(lines))

    results = []
    for l in lines:
        s1, s2 = Styles.from_str(l[0]), Styles.from_str(l[1])
        if s1.is_winning(s2):
            results.append(s1.value)
        else:
            results.append(s2.value)

    return results
