from typing import Iterable, Iterator, List, Optional
from enum import Enum
import math


class Styles(Enum):
    ROCK = "R"
    PAPER = "P"
    SCISSOR = "S"

    @classmethod
    def from_str(cls, s: str) -> "Styles":
        s = s.lower()
        if s == "r":
            return cls.ROCK
        elif s == "p":
            return cls.PAPER
        elif s == "s":
            return cls.SCISSOR

        assert False

    def is_winning(self, other: "Styles") -> bool:
        if self == self.ROCK:
            return other == self.SCISSOR
        if self == self.PAPER:
            return other == self.ROCK
        if self == self.SCISSOR:
            return other == self.PAPER

        assert False


def simulate_all(fighters: List[Styles]) -> Styles:
    while len(fighters) > 1:
        temp = simulate_round(fighters)
        fighters = temp
    return fighters[0]


def simulate_round(fighters: List[Styles]) -> List[Styles]:
    remaining_fighters = []

    for i in range(0, len(fighters), 2):
        s1, s2 = fighters[i], fighters[i + 1]
        if s1.is_winning(s2):
            remaining_fighters.append(s1)
        else:
            remaining_fighters.append(s2)

    return remaining_fighters


def create_rock_pairings(
    remaining_papers: int,
    rock_number: int,
    count: int,
) -> List[Styles]:
    assert rock_number > 0
    results = []
    for i in range(count):
        for _ in range(rock_number):
            results.append(Styles.ROCK)

        if i < remaining_papers:
            results.append(Styles.PAPER)
        else:
            results.append(Styles.SCISSOR)
    return results


def create_pairings(
    nr_of_rocks: int,
    nr_of_papers: int,
    nr_of_scissors: int,
) -> Optional[List[Styles]]:
    N = nr_of_papers + nr_of_rocks + nr_of_scissors

    results = []

    for R in range(int(math.log2(N))):
        rock_number = N // 2**R - 1
        count = nr_of_rocks // rock_number

        results.append(create_rock_pairings(nr_of_papers, rock_number, count))

        nr_of_rocks -= count * rock_number
        if count >= nr_of_papers:
            nr_of_scissors -= count - nr_of_papers
            nr_of_papers = 0
        else:
            nr_of_papers -= count

        assert nr_of_papers >= 0
        assert nr_of_rocks >= 0

    assert nr_of_papers >= 0
    assert nr_of_rocks >= 0
    assert nr_of_scissors >= 0

    results.append([Styles.ROCK for _ in range(nr_of_rocks)])
    results.append([Styles.PAPER for _ in range(nr_of_papers)])
    results.append([Styles.SCISSOR for _ in range(nr_of_scissors)])

    flat_results = [style for sub_list in results for style in sub_list]
    assert len(flat_results) == N

    winner = simulate_all(flat_results)
    if winner != Styles.SCISSOR:
        print("Fail")
        assert False
    return flat_results


def run(lines: Iterator[str]) -> Iterable[str]:
    next(lines)

    results = []

    for tournament in lines:
        r, p, s = [int(count[:-1]) for count in tournament.split()]
        pairing = create_pairings(r, p, s)

        assert pairing is not None
        assert pairing.count(Styles.ROCK) == r
        assert pairing.count(Styles.PAPER) == p
        assert pairing.count(Styles.SCISSOR) == s

        results.append("".join([s.value for s in pairing]))

    return results
