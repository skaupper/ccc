from typing import Iterable, Iterator, List, Optional
from enum import Enum

import sys

sys.setrecursionlimit(1000)


class Styles(Enum):
    ROCK = "R"
    PAPER = "P"
    SCISSOR = "S"

    @classmethod
    def from_str(cls, s: str) -> "Styles":
        s = s.lower()
        if s == "r":
            return cls.ROCK
        elif s == "p":
            return cls.PAPER
        elif s == "s":
            return cls.SCISSOR

        print(s)
        assert False

    def is_winning(self, other: "Styles") -> bool:
        if self == self.ROCK:
            return other == self.SCISSOR
        if self == self.PAPER:
            return other == self.ROCK
        if self == self.SCISSOR:
            return other == self.PAPER

        assert False


def simulate_round(fighters: List[Styles]) -> List[Styles]:
    remaining_fighters = []

    for i in range(0, len(fighters), 2):
        s1, s2 = fighters[i], fighters[i + 1]
        if s1.is_winning(s2):
            remaining_fighters.append(s1)
        else:
            remaining_fighters.append(s2)

    return remaining_fighters


CACHE = {}


def create_pairings(
    nr_of_rocks: int,
    nr_of_papers: int,
    nr_of_scissors: int,
) -> Optional[List[Styles]]:
    nr_of_triple_rocks = nr_of_rocks // 3

    results = [  #
        [Styles.ROCK, Styles.ROCK, Styles.ROCK, Styles.PAPER]
        for _ in range(nr_of_triple_rocks)
    ]

    nr_of_rocks -= nr_of_triple_rocks * 3
    nr_of_papers -= nr_of_triple_rocks

    if nr_of_rocks > 0:
        results.append([Styles.ROCK, Styles.PAPER])
        nr_of_rocks -= 1
        nr_of_papers -= 1

    if nr_of_rocks > 0:
        results.append([Styles.ROCK])
        nr_of_rocks -= 1

        if nr_of_papers > 0:
            results.append([Styles.PAPER])
            nr_of_papers -= 1
        else:
            results.append([Styles.SCISSOR])
            nr_of_scissors -= 1

    assert nr_of_papers >= 0
    assert nr_of_rocks >= 0
    assert nr_of_scissors >= 0

    results.append([Styles.PAPER for _ in range(nr_of_papers)])
    results.append([Styles.SCISSOR for _ in range(nr_of_scissors)])

    flat_results = [style for sub_list in results for style in sub_list]
    return flat_results

    # global CACHE

    # cache_key = (nr_of_rocks, nr_of_papers, nr_of_scissors, tuple(temp_pairing))
    # if cache_key in CACHE:
    #     return CACHE[cache_key]

    # if nr_of_rocks + nr_of_papers + nr_of_scissors == 0:
    #     round1 = simulate_round(temp_pairing)
    #     round2 = simulate_round(round1)

    #     if Styles.ROCK in round2 or Styles.SCISSOR not in round2:
    #         CACHE[cache_key] = None
    #         return None

    #     CACHE[cache_key] = temp_pairing
    #     return temp_pairing

    # if nr_of_rocks > 0:
    #     pairing = create_pairings(
    #         nr_of_rocks - 1,
    #         nr_of_papers,
    #         nr_of_scissors,
    #         temp_pairing + [Styles.ROCK],
    #     )
    #     if pairing is not None:
    #         CACHE[cache_key] = pairing
    #         return pairing

    # if nr_of_papers > 0:
    #     pairing = create_pairings(
    #         nr_of_rocks,
    #         nr_of_papers - 1,
    #         nr_of_scissors,
    #         temp_pairing + [Styles.PAPER],
    #     )
    #     if pairing is not None:
    #         CACHE[cache_key] = pairing
    #         return pairing

    # if nr_of_scissors > 0:
    #     pairing = create_pairings(
    #         nr_of_rocks,
    #         nr_of_papers,
    #         nr_of_scissors - 1,
    #         temp_pairing + [Styles.SCISSOR],
    #     )
    #     if pairing is not None:
    #         CACHE[cache_key] = pairing
    #         return pairing

    # CACHE[cache_key] = None
    # return None


def run(lines: Iterator[str]) -> Iterable[str]:
    global CACHE
    next(lines)

    results = []

    for tournament in lines:
        CACHE = {}
        r, p, s = tournament.split()
        pairing = create_pairings(int(r[:-1]), int(p[:-1]), int(s[:-1]))
        assert pairing is not None
        results.append("".join([s.value for s in pairing]))

    return results
