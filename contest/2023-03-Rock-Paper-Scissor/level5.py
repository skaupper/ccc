import copy
from typing import Iterable, Iterator, List, Optional, Tuple
from enum import Enum
from dataclasses import dataclass
import math


class Styles(Enum):
    ROCK = "R"
    PAPER = "P"
    SCISSOR = "S"
    SPOCK = "Y"
    LIZARD = "L"

    @classmethod
    def from_str(cls, s: str) -> "Styles":
        s = s.lower()
        if s == "r":
            return cls.ROCK
        elif s == "p":
            return cls.PAPER
        elif s == "s":
            return cls.SCISSOR
        elif s == "y":
            return cls.SPOCK
        elif s == "l":
            return cls.LIZARD

        assert False

    def is_winning(self, other: "Styles") -> bool:
        if self == self.ROCK:
            return other in [self.SCISSOR, self.LIZARD]
        if self == self.PAPER:
            return other in [self.ROCK, self.SPOCK]
        if self == self.SCISSOR:
            return other in [self.PAPER, self.LIZARD]
        if self == self.SPOCK:
            return other in [self.ROCK, self.SCISSOR]
        if self == self.LIZARD:
            return other in [self.PAPER, self.SPOCK]

        assert False


@dataclass
class StyleCounts:
    nr_of_rocks: int
    nr_of_papers: int
    nr_of_scissors: int
    nr_of_spocks: int
    nr_of_lizards: int

    def __getitem__(self, style: Styles) -> int:
        if style == Styles.ROCK:
            return self.nr_of_rocks
        if style == Styles.PAPER:
            return self.nr_of_papers
        if style == Styles.SCISSOR:
            return self.nr_of_scissors
        if style == Styles.SPOCK:
            return self.nr_of_spocks
        if style == Styles.LIZARD:
            return self.nr_of_lizards
        assert False


def simulate_all(fighters: List[Styles]) -> Styles:
    while len(fighters) > 1:
        temp = simulate_round(fighters)
        fighters = temp
    return fighters[0]


def simulate_round(fighters: List[Styles]) -> List[Styles]:
    remaining_fighters = []

    for i in range(0, len(fighters), 2):
        s1, s2 = fighters[i], fighters[i + 1]
        if s1.is_winning(s2):
            remaining_fighters.append(s1)
        else:
            remaining_fighters.append(s2)

    return remaining_fighters


def eliminate_rocks(
    style_counts: StyleCounts,
    N: int,
    R: int,
) -> List[Styles]:
    rock_number = N // 2**R - 2
    count = style_counts.nr_of_rocks // rock_number

    results = []
    for _ in range(count):
        for _ in range(rock_number):
            results.append(Styles.ROCK)
            style_counts.nr_of_rocks -= 1

        if style_counts.nr_of_spocks > 0:
            results.append(Styles.SPOCK)
            style_counts.nr_of_spocks -= 1
        elif style_counts.nr_of_papers > 0:
            results.append(Styles.PAPER)
            style_counts.nr_of_papers -= 1
        else:
            assert False

    return results


def eliminate_spocks(
    style_counts: StyleCounts,
    N: int,
    R: int,
) -> List[Styles]:
    spock_number = N // 2**R - 2
    count = style_counts.nr_of_spocks // spock_number

    results = []
    for _ in range(count):
        for _ in range(spock_number):
            results.append(Styles.SPOCK)
            style_counts.nr_of_spocks -= 1

        if style_counts.nr_of_papers > 0:
            results.append(Styles.PAPER)
            style_counts.nr_of_papers -= 1
        elif style_counts.nr_of_lizards > 0:
            results.append(Styles.LIZARD)
            style_counts.nr_of_lizards -= 1
        else:
            print(style_counts)
            assert False

        if style_counts.nr_of_papers > 0:
            results.append(Styles.PAPER)
            style_counts.nr_of_papers -= 1
        elif style_counts.nr_of_lizards > 0:
            results.append(Styles.LIZARD)
            style_counts.nr_of_lizards -= 1
        else:
            assert False
    return results


def create_pairings(style_counts: StyleCounts) -> Optional[List[Styles]]:
    N = style_counts.nr_of_papers + style_counts.nr_of_rocks + style_counts.nr_of_scissors + style_counts.nr_of_spocks + style_counts.nr_of_lizards

    results = []

    for R in range(1, int(math.log2(N)-1)):
        results.append(eliminate_rocks(style_counts, N, R))
    for R in range(1, int(math.log2(N)-1)):
        results.append(eliminate_spocks(style_counts, N, R))

    results.append([Styles.ROCK for _ in range(style_counts.nr_of_rocks)])
    results.append([Styles.SPOCK for _ in range(style_counts.nr_of_spocks)])
    results.append([Styles.PAPER for _ in range(style_counts.nr_of_papers)])
    results.append([Styles.LIZARD for _ in range(style_counts.nr_of_lizards)])
    results.append(
        [Styles.SCISSOR for _ in range(style_counts.nr_of_scissors)])

    flat_results = [style for sub_list in results for style in sub_list]
    assert len(flat_results) == N

    winner = simulate_all(flat_results)
    if winner != Styles.SCISSOR:
        print("Fail")
        print("".join([s.value for s in flat_results]))
        assert False
    return flat_results


def run(lines: Iterator[str]) -> Iterable[str]:
    next(lines)

    results = []

    for tournament in lines:
        counts = StyleCounts(
            *[int(count[:-1]) for count in tournament.split()])
        pairing = create_pairings(copy.deepcopy(counts))

        assert pairing is not None
        assert pairing.count(Styles.ROCK) == counts.nr_of_rocks
        assert pairing.count(Styles.PAPER) == counts.nr_of_papers
        assert pairing.count(Styles.SCISSOR) == counts.nr_of_scissors
        assert pairing.count(Styles.SPOCK) == counts.nr_of_spocks
        assert pairing.count(Styles.LIZARD) == counts.nr_of_lizards

        results.append("".join([s.value for s in pairing]))

    return results
