from typing import Iterable, Iterator
from enum import Enum


class Styles(Enum):
    ROCK = "R"
    PAPER = "P"
    SCISSOR = "S"

    @classmethod
    def from_str(cls, s: str) -> "Styles":
        s = s.lower()
        if s == "r":
            return cls.ROCK
        elif s == "p":
            return cls.PAPER
        elif s == "s":
            return cls.SCISSOR

        print(s)
        assert False

    def is_winning(self, other: "Styles") -> bool:
        if self == self.ROCK:
            return other == self.SCISSOR
        if self == self.PAPER:
            return other == self.ROCK
        if self == self.SCISSOR:
            return other == self.PAPER

        assert False


def run(lines: Iterator[str]) -> Iterable[str]:
    next(lines)

    results = []

    for tournament in lines:
        remaining_fighters = []
        next_remaining_fighters = tournament

        for _ in range(2):
            remaining_fighters = next_remaining_fighters
            next_remaining_fighters = []

            for i in range(0, len(remaining_fighters), 2):
                s1, s2 = remaining_fighters[i], remaining_fighters[i+1]
                s1 = Styles.from_str(s1)
                s2 = Styles.from_str(s2)
                if s1.is_winning(s2):
                    next_remaining_fighters.append(s1.value)
                else:
                    next_remaining_fighters.append(s2.value)

        results.append("".join(next_remaining_fighters))

    return results
