from typing import Iterable, Iterator
from enum import Enum

class Direction(Enum):
    North = "W"
    East = "D"
    South = "S"
    West = "A"


def run(lines: Iterator[str]) -> Iterable[str]:
    l = int(next(lines))

    for i in range(0, l):
        line = next(lines)

        w = line.count("W")
        a = line.count("A")
        s = line.count("S")
        d = line.count("D")

        yield f"{w} {d} {s} {a}"

