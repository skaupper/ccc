from typing import Iterable, Iterator
from enum import Enum
from dataclasses import dataclass

class Direction(Enum):
    North = "W"
    East = "D"
    South = "S"
    West = "A"

@dataclass(frozen=True)
class Coordinate:
    x: int
    y: int

def run(lines: Iterator[str]) -> Iterable[str]:
    gardens = int(next(lines))

    for i in range(0, gardens):
        line = next(lines)
        
        xdir = 0
        ydir = 0
        xmin = 0
        xmax = 0
        ymin = 0
        ymax = 0
        for c in line:
            if c == "W":
                ydir -= 1
            elif c == "S":
                ydir += 1
            elif c == "A":
                xdir -= 1
            elif c == "D":
                xdir += 1

            xmax = max(xmax, xdir)
            ymax = max(ymax, ydir)
            xmin = min(xmin, xdir)
            ymin = min(ymin, ydir)

        yield f"{xmax - xmin + 1} {ymax - ymin + 1}"

