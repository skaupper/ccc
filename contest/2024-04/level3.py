from typing import Iterable, Iterator
from enum import Enum
from dataclasses import dataclass

class Direction(Enum):
    North = "W"
    East = "D"
    South = "S"
    West = "A"

@dataclass(frozen=True)
class Coordinate:
    x: int
    y: int

def run(lines: Iterator[str]) -> Iterable[str]:
    gardens = int(next(lines))

    for i in range(0, gardens):
        line = next(lines)
        skip = False
        width, height = map(int, line.strip().split(" "))
        visitedPoints = set()
        visitedPoints.add(Coordinate(0,0))
        treePositions = []

        for y in range(0, height):
            lawnRow = next(lines)
            treePosX = lawnRow.find("X")
            
            if treePosX != -1:
                treePositions.append(Coordinate(treePosX, y))

        xdir = 0
        ydir = 0
        xmin = 0
        xmax = 0
        ymin = 0
        ymax = 0

        path = next(lines)
        for c in path:
            if c == "W":
                ydir -= 1
            elif c == "S":
                ydir += 1
            elif c == "A":
                xdir -= 1
            elif c == "D":
                xdir += 1

            cord = Coordinate(xdir, ydir)
            if cord in visitedPoints:
                skip = True
                break

            visitedPoints.add(cord)

            xmax = max(xmax, xdir)
            ymax = max(ymax, ydir)
            xmin = min(xmin, xdir)
            ymin = min(ymin, ydir)

        if not skip:
            for tree in treePositions:
                treePos = Coordinate(xmin + tree.x, ymin + tree.y)
                if treePos in visitedPoints:
                    #yield "INVALID"
                    skip = True
                    break

                visitedPoints.add(treePos)
            
        if not skip:
            if len(visitedPoints) != width * height:
                #yield "INVALID"
                skip = True
        
        if not skip:
            if width != xmax - xmin + 1 or height != ymax - ymin + 1:
                skip = True

        if skip:
            yield "INVALID"
        else:
            yield "VALID"
