from typing import Iterable, Iterator
from enum import Enum
from dataclasses import dataclass
from typing import Optional

class Direction(Enum):
    North = "W"
    East = "D"
    South = "S"
    West = "A"

@dataclass(frozen=True)
class Coordinate:
    x: int
    y: int

@dataclass
class hamiltonPath:
    cachedPaths: set[tuple[Coordinate, set[Coordinate]]]
    width: int
    height: int

    def hamiltonianPath(
        self,
        currentPositin: Coordinate,
        visitedPoints: set[Coordinate]
    ) -> Optional[list[Direction]]:
        #if (currentPositin, frozenset(visitedPoints)) in self.cachedPaths:
        #    return None

        if not (0 <= currentPositin.x < self.width) or not(0 <= currentPositin.y < self.height):
            return None
        
        if currentPositin in visitedPoints:
            return None

        #visitedPointsCopy = visitedPoints.copy()
        visitedPoints.add(currentPositin)
        if len(visitedPoints) == self.width * self.height:
            return []

        valueUp = self.hamiltonianPath(Coordinate(currentPositin.x, currentPositin.y - 1), visitedPoints)
        if valueUp is not None:
            return valueUp + [Direction.North]
        
        valueDown = self.hamiltonianPath(Coordinate(currentPositin.x, currentPositin.y + 1), visitedPoints)
        if valueDown is not None:
            return valueDown + [Direction.South]
        
        valueRight = self.hamiltonianPath(Coordinate(currentPositin.x + 1, currentPositin.y), visitedPoints)
        if valueRight is not None:
            return valueRight + [Direction.East]
        
        valueLeft = self.hamiltonianPath(Coordinate(currentPositin.x - 1, currentPositin.y), visitedPoints)
        if valueLeft is not None:
            return valueLeft + [Direction.West]

        visitedPoints.remove(currentPositin)
        #self.cachedPaths.add((currentPositin, frozenset(visitedPoints)))
        return None


def run(lines: Iterator[str]) -> Iterable[str]:
    gardens = int(next(lines))

    for i in range(0, gardens):
        line = next(lines)
        skip = False
        width, height = map(int, line.strip().split(" "))
        visitedPoints = set()

        for y in range(0, height):
            lawnRow = next(lines)
            treePosX = lawnRow.find("X")
            
            if treePosX != -1:
                visitedPoints.add(Coordinate(treePosX, y))

        hp = hamiltonPath(set(), width, height)

        path = hp.hamiltonianPath(Coordinate(0,0), visitedPoints)
        if path is not None:
            path.reverse()

        s = "".join([dir.value for dir in path])
        print(s)
        yield s