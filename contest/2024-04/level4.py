from typing import Iterable, Iterator
from enum import Enum
from dataclasses import dataclass
from typing import Optional, Tuple, List, Any

class Direction(Enum):
    North = "W"
    East = "D"
    South = "S"
    West = "A"

@dataclass(frozen=True)
class Coordinate:
    x: int
    y: int


@dataclass(frozen=True)
class Rectangle:
    bottomRight: Coordinate
    topLeft: Coordinate

    # Get all hamiltonian paths in a graph from a given start vertex
# Python implementation

from typing import Dict, Set, List
from collections import defaultdict


class Graph:
    def __init__(self, edges):
        self.edges = edges
        self.graph: Dict[Coordinate, Set] = defaultdict(set)
        self.visited = defaultdict(lambda: False)

        self.make_graph(edges)

        self.visited_nodes = 0
        self.total_nodes = len(self.graph.keys())

    def make_graph(self, _edges) -> None:
        self.edges.extend(_edges)

        for u, v in self.edges:
            self.graph[u].add(v)
            self.graph[v].add(u)

    def visit(self, node):
        self.visited[node] = True
        self.visited_nodes += 1

    def un_visit(self, node):
        self.visited[node] = False
        self.visited_nodes -= 1

    def all_nodes_are_visited(self) -> bool:
        return self.visited_nodes == self.total_nodes

    def get_hamiltonian_path(self, start) -> List[List[Coordinate]]:
        self.visit(start)

        all_paths = []

        if self.all_nodes_are_visited():
            all_paths.append([start])

        for node in self.graph[start]:
            if self.visited[node]:
                continue
            paths = self.get_hamiltonian_path(node)
            for path in paths:
                if path:
                    path.append(start)
                    all_paths.append(path)

        self.un_visit(start)
        return all_paths


#if __name__ == '__main__':
#    edges = [
#        (1, 2), (2, 3), (3, 4), (4, 1), (2, 4), (1, 3)
#    ]
#    graph = Graph(edges)
#    hamiltonian_path = graph.get_hamiltonian_path(start=1)
#
#    for path in hamiltonian_path:
#        print("->".join(map(str, reversed(path))))



@dataclass
class hamiltonPath:
    cachedPaths: set[tuple[Coordinate, set[Coordinate]]]
    width: int
    height: int

    def hamiltonianPath(
        self,
        currentPositin: Coordinate,
        visitedPoints: set[Coordinate]
    ) -> Optional[list[Direction]]:
        #if (currentPositin, frozenset(visitedPoints)) in self.cachedPaths:
        #    return None

        if not (0 <= currentPositin.x < self.width) or not(0 <= currentPositin.y < self.height):
            return None
        
        if currentPositin in visitedPoints:
            return None

        #visitedPointsCopy = visitedPoints.copy()
        visitedPoints.add(currentPositin)
        if len(visitedPoints) == self.width * self.height:
            return []

        valueUp = self.hamiltonianPath(Coordinate(currentPositin.x, currentPositin.y - 1), visitedPoints)
        if valueUp is not None:
            return valueUp + [Direction.North]
        
        valueDown = self.hamiltonianPath(Coordinate(currentPositin.x, currentPositin.y + 1), visitedPoints)
        if valueDown is not None:
            return valueDown + [Direction.South]
        
        valueRight = self.hamiltonianPath(Coordinate(currentPositin.x + 1, currentPositin.y), visitedPoints)
        if valueRight is not None:
            return valueRight + [Direction.East]
        
        valueLeft = self.hamiltonianPath(Coordinate(currentPositin.x - 1, currentPositin.y), visitedPoints)
        if valueLeft is not None:
            return valueLeft + [Direction.West]

        visitedPoints.remove(currentPositin)
        #self.cachedPaths.add((currentPositin, frozenset(visitedPoints)))
        return None
    


def findPathWithTree(width: int, height: int, tree: Coordinate) -> list[Direction]:
    (leftRect, topRect, rightRect, bottomRect) = divideLawn(width, height, tree)

    nextRects = {
        bottomRect: (Direction.East, rightRect),
        rightRect: (Direction.North, topRect),
        topRect: (Direction.West, leftRect),
        leftRect: (None, None)
    }


    pathsToFind = [
        (bottomRect, Coordinate(tree.x, height-1), []),
        (bottomRect, Coordinate(tree.x, tree.y+1), []),
    ]

    while len(pathsToFind) != 0:
        (rect, startPos, partialPath) = pathsToFind.pop()
        (exitDir, nextRect) = nextRects[rect]

        if nextRect is not None:
            for exitPos, newPath in findPath(rect, startPos, exitDir):
                if exitDir == Direction.North:
                    exitPos = Coordinate(exitPos.x, exitPos.y-1)
                elif exitDir == Direction.East:
                    exitPos = Coordinate(exitPos.x-1, exitPos.y)
                elif exitDir == Direction.West:
                    exitPos = Coordinate(exitPos.x+1, exitPos.y)
                elif exitDir == Direction.South:
                    exitPos = Coordinate(exitPos.x, exitPos.y+1)
                newPath.append(exitDir)
                pathsToFind.append((nextRect, exitPos, partialPath+newPath))
        else:
            result =  fillRectangle(rect, startPos)
            print(result)
            if result is not None:
                return result[0][-1]
            
    assert False, "No path found"


def divideLawn(width: int, height: int, tree: Coordinate) -> list[Rectangle]:
    leftRect = Rectangle(
        bottomRight=Coordinate(tree.x-1, height-1),
        topLeft=Coordinate(0, 0),
    )
    topRect = Rectangle(
        bottomRight=Coordinate(width-1, tree.y-1),
        topLeft=Coordinate(tree.x, 0),
    )
    rightRect = Rectangle(
        bottomRight=Coordinate(width-1, height-1),
        topLeft=Coordinate(tree.x+1, tree.y),
    )
    bottomRect = Rectangle(
        bottomRight=Coordinate(tree.x, height-1),
        topLeft=Coordinate(tree.x, tree.y+1),
    )
    print(bottomRect)
    return [leftRect, topRect, rightRect, bottomRect]


def findPath(rectangle: Rectangle, entryPosition: Coordinate, exitDir: Direction) -> List[Tuple[Coordinate, list[Direction]]]:
    graph = makeGraph(rectangle)
    paths = graph.get_hamiltonian_path(entryPosition)

    results = []
    if len(paths) == 0:
        return [(entryPosition, [])]
    
    for p in paths:
        lastPos = p[-1]
        directions = toDirections(p)
        results.append((lastPos, directions))

    return results

def fillRectangle(rectangle: Rectangle, entryPosition: Coordinate) -> List[Tuple[Coordinate, list[Direction]]]:
    results = []
    for dir in Direction:
        results += findPath(rectangle, entryPosition, dir)
    return results


def toDirections(path: List[Coordinate]) -> List[Direction]:
    directions = []
    lastNode = path[0]
    for i in range(1, len(path)):
        n = path[i]
        if lastNode.x > n.x:
            directions.append(Direction.West)
        elif lastNode.x < n.x:
            directions.append(Direction.East)
        elif lastNode.y > n.y:
            directions.append(Direction.North)
        elif lastNode.y < n.y:
            directions.append(Direction.South)
        else:
            assert False
        lastNode = path[i]

    return directions


def makeGraph(rectangle: Rectangle) -> Graph:
    edges = []
    for y in range(rectangle.topLeft.y, rectangle.bottomRight.y+1):
        for x in range(rectangle.topLeft.x, rectangle.bottomRight.x+1):
            if x < rectangle.bottomRight.x-1:
                edges.append((Coordinate(x,y), Coordinate(x+1,y)))
            if x > rectangle.topLeft.x:
                edges.append((Coordinate(x,y), Coordinate(x-1,y)))
            if y > rectangle.topLeft.y:
                edges.append((Coordinate(x,y), Coordinate(x,y-1)))
            if y < rectangle.bottomRight.y-1:
                edges.append((Coordinate(x,y), Coordinate(x,y+1)))

    return Graph(edges)
    






def run(lines: Iterator[str]) -> Iterable[str]:
    gardens = int(next(lines))

    for i in range(0, gardens):
        line = next(lines)
        width, height = map(int, line.strip().split(" "))
        tree = None

        for y in range(0, height):
            lawnRow = next(lines)
            treePosX = lawnRow.find("X")
        
            if treePosX != -1:
                tree = Coordinate(treePosX, y)

        assert tree is not None
        directions = findPathWithTree(width, height, tree)

        s = "".join([dir.value for dir in directions])
        print(s)
        yield s