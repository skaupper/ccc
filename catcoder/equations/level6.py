from typing import Iterable, Iterator, List, Dict, Optional, Callable, Union, Tuple
from dataclasses import dataclass, field, replace
from more_itertools import peekable
import re


#
# The segments of the seven segment are numbered from top left to bottom right
#   --
#  |  |
#   --
#  |  |
#   --
#
@dataclass(frozen=True)
class SevenSegmentDisplay:
    segments: List[bool]

    def print(self) -> None:
        segs = iter(self.segments)

        def print_horizontal():
            print(" {} ".format("--" if next(segs) else ""))

        def print_vertical():
            left = next(segs)
            right = next(segs)
            print("{}  {}".format("|" if left else " ", "|" if right else " "))

        print_horizontal()
        print_vertical()
        print_horizontal()
        print_vertical()
        print_horizontal()


NUMBERS = {
    0: SevenSegmentDisplay(segments=[True, True, True, False, True, True, True]),
    1: SevenSegmentDisplay(segments=[False, False, True, False, False, True, False]),
    2: SevenSegmentDisplay(segments=[True, False, True, True, True, False, True]),
    3: SevenSegmentDisplay(segments=[True, False, True, True, False, True, True]),
    4: SevenSegmentDisplay(segments=[False, True, True, True, False, True, False]),
    5: SevenSegmentDisplay(segments=[True, True, False, True, False, True, True]),
    6: SevenSegmentDisplay(segments=[True, True, False, True, True, True, True]),
    7: SevenSegmentDisplay(segments=[True, False, True, False, False, True, False]),
    8: SevenSegmentDisplay(segments=[True, True, True, True, True, True, True]),
    9: SevenSegmentDisplay(segments=[True, True, True, True, False, True, True]),
}


@dataclass(frozen=True)
class Digit:
    value: int
    fixed: bool = False

    def to_str(self) -> str:
        return str(self.value)


@dataclass(frozen=True)
class Operator:
    operator: str
    fixed: bool = False

    def to_str(self) -> str:
        return self.operator


@dataclass(frozen=True)
class Expression:
    terms_and_operators: List[Union[Digit, Operator]]


@dataclass(frozen=True)
class MatchDifference:
    removals: int
    additions: int


def determine_transitions() -> Dict[int, Dict[int, MatchDifference]]:

    def to_idx_array(n) -> List[int]:
        return [idx for (idx, seg) in enumerate(NUMBERS[n].segments) if seg]

    transitions = {}

    for from_int in range(10):
        transitions[from_int] = {}

        for to_int in range(10):
            from_array = to_idx_array(from_int)
            to_array = to_idx_array(to_int)

            removals = len(set(from_array).difference(to_array))
            additions = len(set(to_array).difference(from_array))

            transitions[from_int][to_int] = MatchDifference(removals, additions)

    return transitions


TRANSITIONS = determine_transitions()
OPERATOR_TRANSITIONS = {
    "+": {
        "+": MatchDifference(removals=0, additions=0),
        "-": MatchDifference(removals=1, additions=0),
        "=": MatchDifference(removals=1, additions=1),
    },
    "-": {
        "+": MatchDifference(removals=0, additions=1),
        "-": MatchDifference(removals=0, additions=0),
        "=": MatchDifference(removals=0, additions=1),
    },
    "=": {
        "+": MatchDifference(removals=1, additions=1),
        "-": MatchDifference(removals=1, additions=0),
        "=": MatchDifference(removals=0, additions=0),
    }
}
OPERATORS = {
    "+": lambda a, b: a + b,
    "-": lambda a, b: a - b,
}


def parse_expression(line_part: str) -> Expression:
    to: List[Union[Digit, Operator]] = list()

    for s in re.split(r'(\+|-|=)', line_part):
        if s == "":
            to.append(Digit(value=0, fixed=True))
        elif s in ["+", "-", "="]:
            to.append(Operator(operator=s, fixed=False))
        else:
            for d in s:
                to.append(Digit(value=int(d), fixed=False))

    return Expression(to)


@dataclass(frozen=True)
class RecurseResult:
    value: int
    lhs_parts: List[str]
    rhs_parts: List[str]


@dataclass(frozen=True)
class RecursionState:
    expression: Expression
    idx: int = 0
    removals: int = 0
    additions: int = 0
    parts: List[Union[Operator, Digit]] = field(default_factory=list)

    def is_exhausted(self) -> bool:
        return self.idx >= len(self.expression.terms_and_operators)


def _process_state(state: RecursionState) -> Optional[RecurseResult]:
    current = state.expression.terms_and_operators[state.idx]

    def _call_recurse(part: Union[Operator, Digit], differences: MatchDifference) -> Optional[RecurseResult]:
        new_state = replace(
            state,
            idx=state.idx + 1,
            parts=state.parts + [part],
            removals=state.removals + differences.removals,
            additions=state.additions + differences.additions
        )
        result = recurse(new_state)
        if new_state.removals <= 1 and new_state.additions <= 1:
            return result

    possible_results = []
    if isinstance(current, Operator):
        for (op, op_differences) in OPERATOR_TRANSITIONS[current.operator].items():
            if current.fixed and op != current.operator:
                continue
            result = _call_recurse(Operator(op, fixed=current.fixed), op_differences)
            if result is not None:
                possible_results.append(result)
    elif isinstance(current, Digit):
        for (value, value_differences) in TRANSITIONS[current.value].items():
            if current.fixed and value != current.value:
                continue
            result = _call_recurse(Digit(value, fixed=current.fixed), value_differences)
            if result is not None:
                possible_results.append(result)
    else:
        assert False

    unique_results = set([result.value for result in possible_results])
    if len(unique_results) == 0:
        return None
    if len(unique_results) > 1:
        print(f"Multiple solutions!")
    return possible_results[0]


def _split_parts(
    parts: Iterable[Union[Operator, Digit]]
) -> Tuple[List[Union[Operator, Digit]], List[Union[Operator, Digit]]]:
    lhs = []
    rhs = []

    curr = lhs
    for p in parts:
        if p.to_str() == "=":
            curr = rhs
            continue
        curr.append(p)

    return (lhs, rhs)


def get_digit_val(parts: peekable) -> Optional[int]:
    next = parts.peek(None)
    val = 0

    if next is None or not isinstance(next, Digit):
        return None

    while next is not None and isinstance(next, Digit):
        parts.next()
        if not next.fixed:
            val *= 10
            val += next.value

        next = parts.peek(None)

    return val


def _evaluate_expression(parts: Iterable[Union[Operator, Digit]]) -> Optional[int]:
    parts = peekable(parts)
    value = get_digit_val(parts)
    assert value is not None

    while True:
        operator = next(parts, None)
        if operator is None:
            break
        operator = operator.to_str()
        operand = get_digit_val(parts)
        assert operand is not None
        value = OPERATORS[operator](value, operand)
    return value


def recurse(state: RecursionState) -> Optional[RecurseResult]:
    # Exit as soon as both sides are exhausted
    if state.is_exhausted():
        if (state.removals != state.additions) or state.removals > 1:
            return None
        if state.parts.count(Operator(operator="=")) != 1:
            return None
        lhs, rhs = _split_parts(state.parts)
        lhs_value = _evaluate_expression(lhs)
        rhs_value = _evaluate_expression(rhs)
        if lhs_value != rhs_value or lhs_value is None or rhs_value is None:
            return None
        return RecurseResult(
            lhs_value, [p.to_str() for p in lhs if not p.fixed], [p.to_str() for p in rhs if not p.fixed]
        )

    return _process_state(state)


def run(lines: Iterator[str]) -> Iterable[str]:
    for l in lines:
        expression = parse_expression(l.strip())
        result = recurse(RecursionState(expression))
        assert result is not None, f"Could not find a solution for the given equation: {l}"
        yield f"{''.join(result.lhs_parts)}={''.join(result.rhs_parts)}"
