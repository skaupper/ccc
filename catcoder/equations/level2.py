from typing import Iterable, Iterator, List, Dict, Optional
from dataclasses import dataclass
import re


#
# The segments of the seven segment are numbered from top left to bottom right
#   --
#  |  |
#   --
#  |  |
#   --
#
@dataclass(frozen=True)
class SevenSegmentDisplay:
    segments: List[bool]

    def print(self) -> None:
        segs = iter(self.segments)

        def print_horizontal():
            print(" {} ".format("--" if next(segs) else ""))

        def print_vertical():
            left = next(segs)
            right = next(segs)
            print("{}  {}".format("|" if left else " ", "|" if right else " "))

        print_horizontal()
        print_vertical()
        print_horizontal()
        print_vertical()
        print_horizontal()


NUMBERS = {
    0: SevenSegmentDisplay(segments=[True, True, True, False, True, True, True]),
    1: SevenSegmentDisplay(segments=[False, False, True, False, False, True, False]),
    2: SevenSegmentDisplay(segments=[True, False, True, True, True, False, True]),
    3: SevenSegmentDisplay(segments=[True, False, True, True, False, True, True]),
    4: SevenSegmentDisplay(segments=[False, True, True, True, False, True, False]),
    5: SevenSegmentDisplay(segments=[True, True, False, True, False, True, True]),
    6: SevenSegmentDisplay(segments=[True, True, False, True, True, True, True]),
    7: SevenSegmentDisplay(segments=[True, False, True, False, False, True, False]),
    8: SevenSegmentDisplay(segments=[True, True, True, True, True, True, True]),
    9: SevenSegmentDisplay(segments=[True, True, True, True, False, True, True]),
}


def determine_transitions() -> Dict[int, List[int]]:

    def to_idx_array(n) -> List[int]:
        return [idx for (idx, seg) in enumerate(NUMBERS[n].segments) if seg]

    transitions = {}

    for from_int in range(10):
        transitions[from_int] = []

        for to_int in range(10):
            from_array = to_idx_array(from_int)
            to_array = to_idx_array(to_int)

            diff_from = set(from_array).difference(to_array)
            diff_to = set(to_array).difference(from_array)
            if len(diff_from) != 1 or len(diff_to) != 1:
                continue

            transitions[from_int].append(to_int)

    return transitions


@dataclass(frozen=True)
class Expression:
    terms: List[int]
    operators: List[str]


TRANSITIONS = determine_transitions()
OPERATORS = {
    "+": lambda a, b: a + b,
    "-": lambda a, b: a - b,
}


def parse_expression(line_part: str) -> Expression:
    terms = list(map(int, re.split(r'\+|-', line_part)))
    operators = [s for s in line_part if not s.isdigit()]
    return Expression(terms, operators)


@dataclass
class RecurseResult:
    value: int
    lhs_parts: List[str]
    rhs_parts: List[str]

    def prepend_lhs_part(self, part: str) -> None:
        self.lhs_parts.insert(0, part)

    def prepend_rhs_part(self, part: str) -> None:
        self.rhs_parts.insert(0, part)


def recurse(lhs_expr, lhs_term_idx, lhs_curr_sum, rhs_expr, rhs_term_idx, rhs_curr_sum) -> Optional[RecurseResult]:
    # Exit as soon as both sides are exhausted
    if lhs_term_idx == len(lhs_expr.terms) and rhs_term_idx == len(rhs_expr.terms):
        return RecurseResult(value=lhs_curr_sum, lhs_parts=[], rhs_parts=[]) if lhs_curr_sum == rhs_curr_sum else None

    # Try iterating over all possibilities on the LHS
    if lhs_term_idx < len(lhs_expr.terms):
        curr_term = lhs_expr.terms[lhs_term_idx]
        curr_op = "+" if lhs_term_idx == 0 else lhs_expr.operators[lhs_term_idx - 1]

        possible_values = [curr_term] + TRANSITIONS[curr_term]
        possible_results = []
        for v in possible_values:
            result = recurse(
                lhs_expr, lhs_term_idx + 1, OPERATORS[curr_op](lhs_curr_sum, v), rhs_expr, rhs_term_idx, rhs_curr_sum
            )
            if result is not None:
                result.prepend_lhs_part(str(v))
                if lhs_term_idx > 0:
                    result.prepend_lhs_part(str(curr_op))
                possible_results.append(result)

        unique_results = set([result.value for result in possible_results])
        if len(unique_results) == 0:
            return None
        assert len(unique_results) == 1, f"The equation has multiple solutions?"
        return possible_results[0]

    # Try iterating over all possibilities on the rhs
    if rhs_term_idx < len(rhs_expr.terms):
        curr_term = rhs_expr.terms[rhs_term_idx]
        curr_op = "+" if rhs_term_idx == 0 else rhs_expr.operators[rhs_term_idx - 1]

        possible_values = [curr_term] + TRANSITIONS[curr_term]
        possible_results = []
        for v in possible_values:
            result = recurse(
                lhs_expr, lhs_term_idx, lhs_curr_sum, rhs_expr, rhs_term_idx + 1, OPERATORS[curr_op](rhs_curr_sum, v)
            )
            if result is not None:
                result.prepend_rhs_part(str(v))
                if rhs_term_idx > 0:
                    result.prepend_rhs_part(str(curr_op))
                possible_results.append(result)

        unique_results = set([result.value for result in possible_results])
        if len(unique_results) == 0:
            return None
        assert len(unique_results) == 1, f"The equation has multiple solutions?"
        return possible_results[0]

    assert False


def run(lines: Iterator[str]) -> Iterable[str]:
    for l in lines:
        (left, right) = map(parse_expression, l.strip().split("=", 1))
        result = recurse(left, 0, 0, right, 0, 0)
        assert result is not None, f"Could not find a solution for the given equation: {l}"
        yield f"{''.join(result.lhs_parts)}={''.join(result.rhs_parts)}"
