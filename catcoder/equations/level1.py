from typing import Iterable, Iterator, List, Dict
from dataclasses import dataclass


#
# The segments of the seven segment are numbered from top left to bottom right
#   --
#  |  |
#   --
#  |  |
#   --
#
@dataclass(frozen=True)
class SevenSegmentDisplay:
    segments: List[bool]

    def print(self) -> None:
        segs = iter(self.segments)

        def print_horizontal():
            print(" {} ".format("--" if next(segs) else ""))

        def print_vertical():
            left = next(segs)
            right = next(segs)
            print("{}  {}".format("|" if left else " ", "|" if right else " "))

        print_horizontal()
        print_vertical()
        print_horizontal()
        print_vertical()
        print_horizontal()


NUMBERS = {
    0: SevenSegmentDisplay(segments=[True, True, True, False, True, True, True]),
    1: SevenSegmentDisplay(segments=[False, False, True, False, False, True, False]),
    2: SevenSegmentDisplay(segments=[True, False, True, True, True, False, True]),
    3: SevenSegmentDisplay(segments=[True, False, True, True, False, True, True]),
    4: SevenSegmentDisplay(segments=[False, True, True, True, False, True, False]),
    5: SevenSegmentDisplay(segments=[True, True, False, True, False, True, True]),
    6: SevenSegmentDisplay(segments=[True, True, False, True, True, True, True]),
    7: SevenSegmentDisplay(segments=[True, False, True, False, False, True, False]),
    8: SevenSegmentDisplay(segments=[True, True, True, True, True, True, True]),
    9: SevenSegmentDisplay(segments=[True, True, True, True, False, True, True]),
}


def determine_transitions() -> Dict[int, List[int]]:

    def to_idx_array(n) -> List[int]:
        return [idx for (idx, seg) in enumerate(NUMBERS[n].segments) if seg]

    transitions = {}

    for from_int in range(10):
        transitions[from_int] = []

        for to_int in range(10):
            from_array = to_idx_array(from_int)
            to_array = to_idx_array(to_int)

            diff_from = set(from_array).difference(to_array)
            diff_to = set(to_array).difference(from_array)
            if len(diff_from) != 1 or len(diff_to) != 1:
                continue

            transitions[from_int].append(to_int)

    return transitions


TRANSITIONS = determine_transitions()


def run(lines: Iterator[str]) -> Iterable[str]:
    for l in lines:
        (left, right) = map(int, l.strip().split("=", 1))
        yield f"{left}={left}"
