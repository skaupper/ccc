from typing import Iterable, Iterator, List, Dict, Optional, Callable
from dataclasses import dataclass, field, replace
import re


#
# The segments of the seven segment are numbered from top left to bottom right
#   --
#  |  |
#   --
#  |  |
#   --
#
@dataclass(frozen=True)
class SevenSegmentDisplay:
    segments: List[bool]

    def print(self) -> None:
        segs = iter(self.segments)

        def print_horizontal():
            print(" {} ".format("--" if next(segs) else ""))

        def print_vertical():
            left = next(segs)
            right = next(segs)
            print("{}  {}".format("|" if left else " ", "|" if right else " "))

        print_horizontal()
        print_vertical()
        print_horizontal()
        print_vertical()
        print_horizontal()


NUMBERS = {
    0: SevenSegmentDisplay(segments=[True, True, True, False, True, True, True]),
    1: SevenSegmentDisplay(segments=[False, False, True, False, False, True, False]),
    2: SevenSegmentDisplay(segments=[True, False, True, True, True, False, True]),
    3: SevenSegmentDisplay(segments=[True, False, True, True, False, True, True]),
    4: SevenSegmentDisplay(segments=[False, True, True, True, False, True, False]),
    5: SevenSegmentDisplay(segments=[True, True, False, True, False, True, True]),
    6: SevenSegmentDisplay(segments=[True, True, False, True, True, True, True]),
    7: SevenSegmentDisplay(segments=[True, False, True, False, False, True, False]),
    8: SevenSegmentDisplay(segments=[True, True, True, True, True, True, True]),
    9: SevenSegmentDisplay(segments=[True, True, True, True, False, True, True]),
}


@dataclass(frozen=True)
class Term:
    value: int
    fixed: bool


@dataclass(frozen=True)
class Expression:
    terms: List[Term]
    operators: List[str]


@dataclass(frozen=True)
class MatchDifference:
    removals: int
    additions: int


def determine_transitions() -> Dict[int, Dict[int, MatchDifference]]:

    def to_idx_array(n) -> List[int]:
        return [idx for (idx, seg) in enumerate(NUMBERS[n].segments) if seg]

    transitions = {}

    for from_int in range(10):
        transitions[from_int] = {}

        for to_int in range(10):
            from_array = to_idx_array(from_int)
            to_array = to_idx_array(to_int)

            removals = len(set(from_array).difference(to_array))
            additions = len(set(to_array).difference(from_array))

            transitions[from_int][to_int] = MatchDifference(removals, additions)

    return transitions


TRANSITIONS = determine_transitions()
OPERATORS = {
    "+": lambda a, b: a + b,
    "-": lambda a, b: a - b,
}


def parse_expression(line_part: str) -> Expression:
    terms: List[Term] = list()

    for s in re.split(r'\+|-', line_part):
        if s == "":
            terms.append(Term(value=0, fixed=True))
        else:
            terms.append(Term(value=int(s), fixed=False))

    operators = [s for s in line_part if not s.isdigit()]
    return Expression(terms, operators)


@dataclass(frozen=True)
class RecurseResult:
    value: int
    lhs_parts: List[str]
    rhs_parts: List[str]


@dataclass(frozen=True)
class RecursionState:
    expression: Expression
    term_idx: int = 0
    curr_sum: int = 0
    removals: int = 0
    additions: int = 0
    parts: List[str] = field(default_factory=list)

    def is_exhausted(self) -> bool:
        return self.term_idx >= len(self.expression.terms)


def _process_state(state: RecursionState, recurse_fn: Callable[[RecursionState],
                                                               Optional[RecurseResult]]) -> Optional[RecurseResult]:
    curr_term = state.expression.terms[state.term_idx]
    curr_op = "+" if state.term_idx == 0 else state.expression.operators[state.term_idx - 1]

    possible_results = []
    for v in range(10):
        if curr_term.fixed and v != curr_term.value:
            continue
        differences = TRANSITIONS[curr_term.value][v]

        new_parts = []
        if state.term_idx > 0:
            new_parts.append(str(curr_op))
        new_parts.append(str(v))

        new_state = replace(
            state,
            term_idx=state.term_idx + 1,
            curr_sum=OPERATORS[curr_op](state.curr_sum, v),
            parts=state.parts + new_parts,
            removals=state.removals + differences.removals,
            additions=state.additions + differences.additions
        )
        result = recurse_fn(new_state)
        if result is not None and new_state.removals <= 1 and new_state.additions <= 1:
            possible_results.append(result)

    unique_results = set([result.value for result in possible_results])
    if len(unique_results) == 0:
        return None
    if len(unique_results) > 1:
        print(f"Multiple solutions!")
    return possible_results[0]


def recurse(lhs: RecursionState, rhs: RecursionState) -> Optional[RecurseResult]:
    # Exit as soon as both sides are exhausted
    if lhs.is_exhausted() and rhs.is_exhausted():
        removals = lhs.removals + rhs.removals
        additions = lhs.additions + rhs.additions
        if lhs.curr_sum != rhs.curr_sum or (removals != additions) or removals > 1:
            return None
        return RecurseResult(lhs.curr_sum, lhs.parts, rhs.parts)

    if not lhs.is_exhausted():
        return _process_state(lhs, lambda state: recurse(state, rhs))
    elif not rhs.is_exhausted():
        return _process_state(rhs, lambda state: recurse(lhs, state))
    else:
        assert False


def run(lines: Iterator[str]) -> Iterable[str]:
    for l in lines:
        (left, right) = map(parse_expression, l.strip().split("=", 1))
        result = recurse(RecursionState(left), RecursionState(right))
        assert result is not None, f"Could not find a solution for the given equation: {l}"
        yield f"{''.join(result.lhs_parts)}={''.join(result.rhs_parts)}"
