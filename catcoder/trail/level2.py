from typing import Iterable, Iterator, Generator, Tuple, TypeVar, Sequence, List
from dataclasses import dataclass


@dataclass(frozen=True)
class Vector:
    x: int
    y: int

    def rotate_r(self) -> "Vector":
        return Vector(self.y, -self.x)

    def rotate_l(self) -> "Vector":
        return Vector(-self.y, self.x)

    def __mul__(self, other: int) -> "Vector":
        return Vector(self.x * other, self.y * other)

    def __add__(self, other: "Vector") -> "Vector":
        return Vector(self.x + other.x, self.y + other.y)


T = TypeVar("T")


def get_pairs(l: Sequence[T]) -> Generator[Tuple[T, T], None, None]:
    assert len(l) % 2 == 0
    for i in range(len(l) // 2):
        yield (l[2 * i], l[2 * i + 1])


def get_path(commands: List[str]) -> List[Vector]:
    dir = Vector(0, 1)
    all_points = [Vector(0, 0)]

    for seq, reps in get_pairs(commands):
        for _ in range(int(reps)):
            for instr in seq:
                if instr == "L":
                    dir = dir.rotate_l()
                elif instr == "R":
                    dir = dir.rotate_r()
                elif instr == "F":
                    all_points.append(all_points[-1] + dir)
                else:
                    assert False

    return all_points


def get_rectangle(all_points: Sequence[Vector]) -> Tuple[Vector, Vector]:
    x_min, x_max = all_points[0].x, all_points[0].x
    y_min, y_max = all_points[0].y, all_points[0].y

    for p in all_points:
        x_min = min(x_min, p.x)
        x_max = max(x_max, p.x)
        y_min = min(y_min, p.y)
        y_max = max(y_max, p.y)

    return Vector(x_min, y_max), Vector(x_max, y_min)


def run(lines: Iterator[str]) -> Iterable[str]:
    l = next(lines)

    path = get_path(l.split(" ")[1:])
    left_top, right_bottom = get_rectangle(path)
    total_distance = len(path) - 1
    area = abs(left_top.x - right_bottom.x) * abs(left_top.y - right_bottom.y)

    return [f"{total_distance} {area}"]
