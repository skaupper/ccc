from typing import Iterable, Iterator, Generator, Tuple, TypeVar, Sequence, List
from dataclasses import dataclass


@dataclass(frozen=True)
class Vector:
    x: int
    y: int

    def rotate_r(self) -> "Vector":
        return Vector(self.y, -self.x)

    def rotate_l(self) -> "Vector":
        return Vector(-self.y, self.x)

    def __mul__(self, other: int) -> "Vector":
        return Vector(self.x * other, self.y * other)

    def __add__(self, other: "Vector") -> "Vector":
        return Vector(self.x + other.x, self.y + other.y)


@dataclass(frozen=True)
class Edge:
    start: Vector
    end: Vector

    def horizontal_between(self, x: float) -> bool:
        return (self.start.x < x < self.end.x) or \
               (self.start.x > x > self.end.x)

    def vertical_between(self, y: float) -> bool:
        return (self.start.y < y < self.end.y) or \
               (self.start.y > y > self.end.y)


T = TypeVar("T")


def get_pairs(l: Sequence[T]) -> Generator[Tuple[T, T], None, None]:
    assert len(l) % 2 == 0
    for i in range(len(l) // 2):
        yield (l[2 * i], l[2 * i + 1])


def get_path(commands: List[str]) -> List[Vector]:
    dir = Vector(0, 1)
    all_points = [Vector(0, 0)]

    for seq, reps in get_pairs(commands):
        for _ in range(int(reps)):
            for instr in seq:
                if instr == "L":
                    dir = dir.rotate_l()
                elif instr == "R":
                    dir = dir.rotate_r()
                elif instr == "F":
                    all_points.append(all_points[-1] + dir)
                else:
                    assert False

    return all_points


def get_rectangle(all_points: Sequence[Vector]) -> Tuple[Vector, Vector]:
    x_min, x_max = all_points[0].x, all_points[0].x
    y_min, y_max = all_points[0].y, all_points[0].y

    for p in all_points:
        x_min = min(x_min, p.x)
        x_max = max(x_max, p.x)
        y_min = min(y_min, p.y)
        y_max = max(y_max, p.y)

    return Vector(x_min, y_max), Vector(x_max, y_min)


def get_area(all_points: Sequence[Vector]) -> int:
    left_top, right_bottom = get_rectangle(all_points)

    all_edges: List[Edge] = []
    for i in range(1, len(all_points)):
        all_edges.append(Edge(all_points[i - 1], all_points[i]))

    def count_intersections(x: float, y: float) -> Tuple[int, int]:
        intersections_above = 0
        intersections_left = 0
        for e in all_edges:
            if e.horizontal_between(x) and e.start.y < y:
                intersections_above += 1
            if e.vertical_between(y) and e.start.x < x:
                intersections_left += 1
        return intersections_above, intersections_left

    inlay_points = 0

    for y in range(right_bottom.y, left_top.y):
        y += 0.5
        for x in range(left_top.x, right_bottom.x):
            x += 0.5

            above_int, left_int = count_intersections(x, y)
            if above_int % 2 == 1 and left_int % 2 == 1:
                inlay_points += 1

    return inlay_points


# def get_outside_points(all_points: Sequence[Vector]) -> List[Vector]:
#     left_top, right_bottom = get_rectangle(all_points)

#     all_edges: List[Edge] = []
#     for i in range(1, len(all_points)):
#         all_edges.append(Edge(all_points[i - 1], all_points[i]))

#     def count_intersections(x: float, y: float) -> Tuple[int, int]:
#         intersections_above = 0
#         intersections_left = 0
#         for e in all_edges:
#             if e.horizontal_between(x) and e.start.y < y:
#                 intersections_above += 1
#             if e.vertical_between(y) and e.start.x < x:
#                 intersections_left += 1
#         return intersections_above, intersections_left

#     candidate_points: List[Vector] = []

#     for y_raw in range(right_bottom.y, left_top.y):
#         y = y_raw + 0.5
#         for x_raw in range(left_top.x, right_bottom.x):
#             x = x_raw + 0.5

#             above_int, left_int = count_intersections(x, y)
#             if above_int % 2 == 0 or left_int % 2 == 0:
#                 candidate_points += [
#                     Vector(x_raw, y_raw),
#                     Vector(x_raw, y_raw + 1),
#                     Vector(x_raw + 1, y_raw),
#                     Vector(x_raw + 1, y_raw + 1),
#                 ]

#     return list(set([p for p in candidate_points if p not in all_points]))

# def get_pockets(all_points: Sequence[Vector]) -> int:
#     outside_points = get_outside_points(all_points)
#     all_points_set = set(all_points)

#     pockets = 0

#     for p in outside_points:
#         if Vector(p.x - 1, p.y) in all_points_set and \
#            Vector(p.x + 1, p.y) in all_points_set:
#             pockets += 1
#             continue
#         if Vector(p.x, p.y - 1) in all_points_set and \
#            Vector(p.x, p.y + 1) in all_points_set:
#             pockets += 1
#             continue

#     return pockets


def get_pockets(all_points: Sequence[Vector]) -> int:
    left_top, right_bottom = get_rectangle(all_points)

    all_edges: List[Edge] = []
    for i in range(1, len(all_points)):
        all_edges.append(Edge(all_points[i - 1], all_points[i]))

    def count_intersections(x: float, y: float) -> Tuple[int, int, int, int]:
        intersections_above = 0
        intersections_left = 0
        intersections_below = 0
        intersections_right = 0
        for e in all_edges:
            if e.horizontal_between(x) and e.start.y < y:
                intersections_above += 1
            if e.vertical_between(y) and e.start.x < x:
                intersections_left += 1
            if e.horizontal_between(x) and e.start.y > y:
                intersections_below += 1
            if e.vertical_between(y) and e.start.x > x:
                intersections_right += 1
        return intersections_above, intersections_left, intersections_below, intersections_right

    pockets = 0

    for y in range(right_bottom.y, left_top.y):
        y += 0.5
        for x in range(left_top.x, right_bottom.x):
            x += 0.5

            above_int, left_int, below_int, right_int = count_intersections(x, y)
            if above_int % 2 == 1 and left_int % 2 == 1:
                continue

            if (above_int > 0 and below_int > 0) or (left_int > 0 and right_int > 0):
                pockets += 1


    return pockets


def run(lines: Iterator[str]) -> Iterable[str]:
    l = next(lines)

    path = get_path(l.split(" ")[1:])
    left_top, right_bottom = get_rectangle(path)
    total_distance = len(path) - 1
    rect_area = abs(left_top.x - right_bottom.x) * abs(left_top.y -
                                                       right_bottom.y)
    poly_area = get_area(path)
    pockets = get_pockets(path)

    print(pockets)

    return [f"{total_distance} {rect_area} {poly_area} {pockets}"]
