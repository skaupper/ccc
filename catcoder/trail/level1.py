from typing import Iterable, Iterator, List, Generator, Tuple, TypeVar, Sequence
from dataclasses import dataclass


@dataclass(frozen=True)
class Vector:
    x: int
    y: int

    def rotate_r(self) -> "Vector":
        return Vector(self.y, -self.x)

    def rotate_l(self) -> "Vector":
        return Vector(-self.y, self.x)

    def __mul__(self, other: int) -> "Vector":
        return Vector(self.x * other, self.y * other)


T = TypeVar("T")


def get_pairs(l: Sequence[T]) -> Generator[Tuple[T, T], None, None]:
    assert len(l) % 2 == 0
    for i in range(len(l) // 2):
        yield (l[2 * i], l[2 * i + 1])


def run(lines: Iterator[str]) -> Iterable[str]:
    l = next(lines)

    commands = l.split(" ")[1:]
    total_distance: int = 0

    for seq, reps in get_pairs(commands):
        total_distance += seq.count("F") * int(reps)

    return [str(total_distance)]
