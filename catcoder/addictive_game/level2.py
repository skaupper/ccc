from typing import Iterable, Iterator, List, Tuple, Dict


def get_coords(pos: int, row_nr: int, col_nr: int) -> Tuple[int, int]:
    pos = pos - 1
    return pos//col_nr + 1, pos%col_nr + 1


def run(lines: Iterator[str]) -> Iterable[str]:
    results: List[str] = []

    for l in lines:
        numbers = [int(n) for n in l.strip().split()]
        rows, cols = numbers[0:2]

        all_points: Dict[int, List[int]] = {}
        max_color = 0

        for p_idx in range(numbers[2]):
            pos, color = numbers[3 + p_idx*2:5 + p_idx*2]
            if color - 1 not in all_points:
                all_points[color - 1] = []
            all_points[color - 1].append(pos)
            max_color = max(max_color, color - 1)

        distances: List[int] = []
        for i in range(max_color + 1):
            p1 = get_coords(all_points[i][0], rows, cols)
            p2 = get_coords(all_points[i][1], rows, cols)

            distances.append(abs(p1[0] - p2[0]) + abs(p1[1] - p2[1]))

        results.append(" ".join(map(str, distances)))

    return results
