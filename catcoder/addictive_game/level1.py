from typing import Iterable, Iterator, List, Tuple


def get_coords(pos: int, row_nr: int, col_nr: int) -> Tuple[int, int]:
    pos = pos - 1
    return pos//col_nr + 1, pos%col_nr + 1


def run(lines: Iterator[str]) -> Iterable[str]:
    results: List[str] = []

    for l in lines:
        numbers = [int(n) for n in l.strip().split()]

        rows, cols = numbers[0:2]

        # for i in range(1, rows*cols + 1):
        #     print(get_coords(i, rows, cols))

        position_coords: List[int] = []
        for pos in numbers[3:]:
            position_coords += [*get_coords(pos, rows, cols)]

        results.append(" ".join(map(str, position_coords)))

    return results
