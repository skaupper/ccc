from typing import Iterable, Iterator, List, Tuple, Dict, Set, Optional, Union
from enum import Enum
from dataclasses import dataclass, replace

import graphs


def get_coords(pos: int, row_nr: int, col_nr: int) -> Tuple[int, int]:
    pos = pos - 1
    return pos // col_nr, pos % col_nr


class MapEntryType(Enum):
    POINT = "O"
    PATH = "."


@dataclass(frozen=True)
class MapEntry:
    type: MapEntryType
    color: int
    x: int
    y: int
    is_connected: bool = False


class ColorStatus(Enum):
    CONNECTED = 1
    CONNECTABLE = 2
    UNCONNECTABLE = 3


class Move(Enum):
    NORTH = "N"
    SOUTH = "S"
    EAST = "E"
    WEST = "W"

    @classmethod
    def from_str(cls, s: str) -> "Move":
        s = s.lower()
        if s == "n":
            return cls.NORTH
        elif s == "s":
            return cls.SOUTH
        elif s == "e":
            return cls.EAST
        elif s == "w":
            return cls.WEST

        assert False

    def apply_to_coords(self, x: int, y: int) -> Tuple[int, int]:
        if self == self.NORTH:
            return (x, y - 1)
        elif self == self.SOUTH:
            return (x, y + 1)
        elif self == self.WEST:
            return (x - 1, y)
        elif self == self.EAST:
            return (x + 1, y)

        assert False


@dataclass(frozen=True)
class Path:
    color: int
    start_x: int
    start_y: int

    moves: List[Move]

    @classmethod
    def from_raw(cls, line: List[str], start_idx: int, rows: int, cols: int) -> Tuple[int, "Path"]:
        color = int(line[start_idx]) - 1
        start_y, start_x = get_coords(int(line[start_idx + 1]), rows, cols)

        move_cnt = int(line[start_idx + 2])
        moves: List[Move] = []
        for i in range(move_cnt):
            moves.append(Move.from_str(line[start_idx + 3 + i]))
        next_idx = start_idx + 3 + move_cnt

        return next_idx, cls(color=color, start_x=start_x, start_y=start_y, moves=moves)

    def get_path(self, game_map: Dict[Tuple[int, int], MapEntry], rows: int, cols: int) -> Optional[List[MapEntry]]:
        p = game_map[(self.start_y, self.start_x)]
        if not (p.type == MapEntryType.POINT and p.color == self.color):
            return None
        assert p.type == MapEntryType.POINT and p.color == self.color

        x, y = self.start_x, self.start_y
        path_points: Set[Tuple[int, int]] = set()
        path_points.add((x, y))
        path: List[MapEntry] = []

        for i, m in enumerate(self.moves):
            x, y = m.apply_to_coords(x, y)
            if x < 0 or x >= cols:
                return None
            if y < 0 or y >= rows:
                return None

            if (x, y) in path_points:
                return None

            if (y, x) in game_map:
                p = game_map[(y, x)]
                if p.type == MapEntryType.PATH or p.color != self.color or i != len(self.moves) - 1:
                    return None

            path_points.add((x, y))
            path.append(MapEntry(MapEntryType.PATH, self.color, x=x, y=y))

        if (y, x) in game_map:
            p = game_map[(y, x)]
        else:
            p = None
        is_valid = p is not None and p.type == MapEntryType.POINT and p.color == self.color and (x, y) != (
            self.start_x, self.start_y
        )
        if not is_valid:
            return None

        return path[:-1]


def draw_map(rows: int, cols: int, game_map: Dict[Tuple[int, int], MapEntry]):
    import io
    string_builder = io.StringIO()
    for y in range(rows):
        for x in range(cols):
            if (y, x) in game_map:
                string_builder.write("X")
            else:
                string_builder.write(" ")
        string_builder.write("\n")
    print(string_builder.getvalue())


# def parse_all_paths(line: str, start_idx: int) -> List[Path]:
#     paths: List[Path] = []
#     while start_idx < len(line):
#         start_idx, p = Path.from_raw(line, start_idx, rows, cols)
#         paths.append(p)

ALL_POINTS_BY_COLOR = Dict[int, List[MapEntry]]
GAME_MAP = Dict[Tuple[int, int], MapEntry]


def parse_map(line: List[str]) -> Tuple[int, ALL_POINTS_BY_COLOR, GAME_MAP, int, int]:
    number_end_idx = 3 + int(line[2]) * 2
    numbers = [int(n) for n in line[:number_end_idx]]
    rows, cols = numbers[0:2]

    all_points: Dict[int, List[MapEntry]] = {}
    game_map: Dict[Tuple[int, int], MapEntry] = {}
    max_color = 0

    for p_idx in range(numbers[2]):
        pos, color = numbers[3 + p_idx*2:5 + p_idx*2]
        y, x = get_coords(pos, rows, cols)
        point = MapEntry(MapEntryType.POINT, color - 1, x=x, y=y)
        if color - 1 not in all_points:
            all_points[color - 1] = []
        all_points[color - 1].append(point)
        game_map[(y, x)] = point
        max_color = max(max_color, point.color)

    next_idx = numbers[2] * 2 + 4
    path_count = int(line[numbers[2] * 2 + 3])
    paths: List[Path] = []
    for _ in range(path_count):
        next_idx, p = Path.from_raw(line, next_idx, rows, cols)
        paths.append(p)

    for p in paths:
        rendered_path = p.get_path(game_map, rows, cols)
        if rendered_path is None:
            continue

        for cell in rendered_path:
            assert (cell.y, cell.x) not in game_map
            game_map[(cell.y, cell.x)] = cell

        for p in all_points[rendered_path[0].color]:
            game_map[(p.y, p.x)] = replace(game_map[(p.y, p.x)], is_connected=True)
    return next_idx, all_points, game_map, rows, cols


@dataclass
class MapEntryPathFinder:
    game_map: GAME_MAP
    rows: int
    cols: int
    end_point: MapEntry

    def get_neighbours(self, point: MapEntry) -> List[graphs.Edge[MapEntry]]:
        """
        Return all outgoing edges from the given point.
        """
        candidates = [
            MapEntry(MapEntryType.PATH, self.end_point.color, point.x - 1, point.y),
            MapEntry(MapEntryType.PATH, self.end_point.color, point.x + 1, point.y),
            MapEntry(MapEntryType.PATH, self.end_point.color, point.x, point.y - 1),
            MapEntry(MapEntryType.PATH, self.end_point.color, point.x, point.y + 1),
        ]

        edges = [
            graphs.Edge(c)  #
            for c in candidates
            if (0 <= c.x < self.cols) and (0 <= c.y < self.rows) and
            ((c.y, c.x) not in self.game_map or self.game_map[(c.y, c.x)] == self.end_point)
        ]

        return edges

    def get_distance_to_target(self, point: MapEntry) -> float:
        """
        Get the minimum distance from the given point to the target point.
        """
        return abs(point.x - self.end_point.x) + abs(point.y - self.end_point.y)

    def is_target(self, point: MapEntry) -> bool:
        """
        Check if the given point is the target point.
        """
        return self.end_point.x == point.x and self.end_point.y == point.y


def run(lines: Iterator[str]) -> Iterable[str]:
    results: List[str] = []

    for l in lines:
        line = [n.strip() for n in l.strip().split()]

        test_count = int(line[0])
        next_idx = 1

        status_string: List[int] = []
        for _ in range(test_count):
            idx_incr, all_points, game_map, rows, cols = parse_map(line[next_idx:])
            next_idx += idx_incr

            color_status: Dict[int, ColorStatus] = {}
            max_color = 0
            for color in all_points:
                p = all_points[color][0]
                max_color = max(max_color, color)
                if game_map[(p.y, p.x)].is_connected:
                    color_status[color] = ColorStatus.CONNECTED
                    continue

                start_point = game_map[(p.y, p.x)]
                p = all_points[color][1]
                end_point = game_map[(p.y, p.x)]
                protocol = MapEntryPathFinder(game_map, rows, cols, end_point)
                path = graphs.get_shortest_path(start_point, protocol)
                if path is not None:
                    color_status[color] = ColorStatus.CONNECTABLE
                else:
                    color_status[color] = ColorStatus.UNCONNECTABLE

            for i in range(max_color + 1):
                status_string.append(color_status[i].value)

        results.append(" ".join(map(str, status_string)))

    return results
