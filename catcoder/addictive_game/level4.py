from typing import Iterable, Iterator, List, Tuple, Dict, Set, Optional
from enum import Enum
from dataclasses import dataclass


def get_coords(pos: int, row_nr: int, col_nr: int) -> Tuple[int, int]:
    pos = pos - 1
    return pos // col_nr, pos % col_nr


class MapEntryType(Enum):
    POINT = "O"
    PATH = "."


@dataclass(frozen=True)
class MapEntry:
    type: MapEntryType
    color: Optional[int]
    x: int
    y: int


class Move(Enum):
    NORTH = "N"
    SOUTH = "S"
    EAST = "E"
    WEST = "W"

    @classmethod
    def from_str(cls, s: str) -> "Move":
        s = s.lower()
        if s == "n":
            return cls.NORTH
        elif s == "s":
            return cls.SOUTH
        elif s == "e":
            return cls.EAST
        elif s == "w":
            return cls.WEST

        assert False

    def apply_to_coords(self, x: int, y: int) -> Tuple[int, int]:
        if self == self.NORTH:
            return (x, y - 1)
        elif self == self.SOUTH:
            return (x, y + 1)
        elif self == self.WEST:
            return (x - 1, y)
        elif self == self.EAST:
            return (x + 1, y)

        assert False


@dataclass(frozen=True)
class Path:
    color: int
    start_x: int
    start_y: int

    moves: List[Move]

    @classmethod
    def from_raw(cls, line: List[str], start_idx: int, rows: int, cols: int) -> Tuple[int, "Path"]:
        color = int(line[start_idx]) - 1
        start_y, start_x = get_coords(int(line[start_idx + 1]), rows, cols)

        move_cnt = int(line[start_idx + 2])
        moves: List[Move] = []
        for i in range(move_cnt):
            moves.append(Move.from_str(line[start_idx + 3 + i]))
        next_idx = start_idx + 3 + move_cnt

        return next_idx, cls(color=color, start_x=start_x, start_y=start_y, moves=moves)

    def get_path(self, game_map: Dict[Tuple[int, int], MapEntry], rows: int, cols: int) -> Optional[List[MapEntry]]:
        p = game_map[(self.start_y, self.start_x)]
        if not (p.type == MapEntryType.POINT and p.color == self.color):
            return None
        assert p.type == MapEntryType.POINT and p.color == self.color

        x, y = self.start_x, self.start_y
        path_points: Set[Tuple[int, int]] = set()
        path_points.add((x, y))
        path: List[MapEntry] = []

        for i, m in enumerate(self.moves):
            x, y = m.apply_to_coords(x, y)
            if x < 0 or x >= cols:
                return None
            if y < 0 or y >= rows:
                return None

            if (x, y) in path_points:
                return None

            if (y, x) in game_map:
                p = game_map[(y, x)]
                if p.type == MapEntryType.PATH or p.color != self.color or i != len(self.moves) - 1:
                    return None

            path_points.add((x, y))
            path.append(MapEntry(MapEntryType.PATH, self.color, x=x, y=y))

        if (y, x) in game_map:
            p = game_map[(y, x)]
        else:
            p = None
        is_valid = p is not None and p.type == MapEntryType.POINT and p.color == self.color and (x, y) != (
            self.start_x, self.start_y
        )
        if not is_valid:
            return None
        return path


def draw_map(rows: int, cols: int, game_map: Dict[Tuple[int, int], MapEntry]):
    import io
    string_builder = io.StringIO()
    for y in range(rows):
        for x in range(cols):
            if (y, x) in game_map:
                string_builder.write("X")
            else:
                string_builder.write(" ")
        string_builder.write("\n")
    print(string_builder.getvalue())


def run(lines: Iterator[str]) -> Iterable[str]:
    results: List[str] = []

    for l in lines:
        line = [n.strip() for n in l.strip().split()]

        number_end_idx = 3 + int(line[2]) * 2
        numbers = [int(n) for n in line[:number_end_idx]]
        rows, cols = numbers[0:2]

        all_points: Dict[int, List[int]] = {}
        game_map: Dict[Tuple[int, int], MapEntry] = {}
        max_color = 0

        for p_idx in range(numbers[2]):
            pos, color = numbers[3 + p_idx*2:5 + p_idx*2]
            if color - 1 not in all_points:
                all_points[color - 1] = []
            all_points[color - 1].append(pos)
            y, x = get_coords(pos, rows, cols)
            game_map[(y, x)] = MapEntry(MapEntryType.POINT, color - 1, x=x, y=y)
            max_color = max(max_color, color - 1)

        next_idx = numbers[2] * 2 + 4
        paths: List[Path] = []
        while next_idx < len(line):
            next_idx, p = Path.from_raw(line, next_idx, rows, cols)
            paths.append(p)

        for p in paths:
            rendered_path = p.get_path(game_map, rows, cols)
            if rendered_path is None:
                continue

            for cell in rendered_path:
                game_map[(cell.y, cell.x)] = cell

        draw_map(rows, cols, game_map)

    return results
