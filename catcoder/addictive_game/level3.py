from typing import Iterable, Iterator, List, Tuple, Dict, Set
from enum import Enum
from dataclasses import dataclass


def get_coords(pos: int, row_nr: int, col_nr: int) -> Tuple[int, int]:
    pos = pos - 1
    return pos // col_nr, pos % col_nr


class Move(Enum):
    NORTH = "N"
    SOUTH = "S"
    EAST = "E"
    WEST = "W"

    @classmethod
    def from_str(cls, s: str) -> "Move":
        s = s.lower()
        if s == "n":
            return cls.NORTH
        elif s == "s":
            return cls.SOUTH
        elif s == "e":
            return cls.EAST
        elif s == "w":
            return cls.WEST

        assert False

    def apply_to_coords(self, x: int, y: int) -> Tuple[int, int]:
        if self == self.NORTH:
            return (x, y - 1)
        elif self == self.SOUTH:
            return (x, y + 1)
        elif self == self.WEST:
            return (x - 1, y)
        elif self == self.EAST:
            return (x + 1, y)

        assert False


@dataclass(frozen=True)
class Path:
    color: int
    start_x: int
    start_y: int

    moves: List[Move]

    @classmethod
    def from_raw(cls, line: List[str], start_idx: int, rows: int, cols: int) -> Tuple[int, "Path"]:
        color = int(line[start_idx]) - 1
        start_y, start_x = get_coords(int(line[start_idx + 1]), rows, cols)

        move_cnt = int(line[start_idx + 2])
        moves: List[Move] = []
        for i in range(move_cnt):
            moves.append(Move.from_str(line[start_idx + 3 + i]))
        next_idx = start_idx + 3 + move_cnt

        return next_idx, cls(color=color, start_x=start_x, start_y=start_y, moves=moves)

    def check_validity(self, all_points_by_coords: Dict[Tuple[int, int], int], rows: int,
                       cols: int) -> Tuple[bool, int]:
        assert all_points_by_coords[(self.start_y, self.start_x)] == self.color

        x, y = self.start_x, self.start_y
        path_points: Set[Tuple[int, int]] = set()
        path_points.add((x, y))

        for i, m in enumerate(self.moves):
            x, y = m.apply_to_coords(x, y)
            if x < 0 or x >= cols:
                return False, i + 1
            if y < 0 or y >= rows:
                return False, i + 1

            if (x, y) in path_points:
                return False, i + 1

            if (y, x) in all_points_by_coords:
                if all_points_by_coords[(y, x)] != self.color or i != len(self.moves) - 1:
                    return False, i + 2

            path_points.add((x, y))

        is_valid = (y, x) in all_points_by_coords and all_points_by_coords[
            (y, x)] == self.color and (x, y) != (self.start_x, self.start_y)
        return is_valid, len(self.moves)


def run(lines: Iterator[str]) -> Iterable[str]:
    results: List[str] = []

    for l in lines:
        line = [n.strip() for n in l.strip().split()]

        number_end_idx = 3 + int(line[2]) * 2
        numbers = [int(n) for n in line[:number_end_idx]]
        rows, cols = numbers[0:2]

        all_points: Dict[int, List[int]] = {}
        all_points_by_coords: Dict[Tuple[int, int], int] = {}
        max_color = 0

        for p_idx in range(numbers[2]):
            pos, color = numbers[3 + p_idx*2:5 + p_idx*2]
            if color - 1 not in all_points:
                all_points[color - 1] = []
            all_points[color - 1].append(pos)
            all_points_by_coords[get_coords(pos, rows, cols)] = color - 1
            max_color = max(max_color, color - 1)

        next_idx = numbers[2] * 2 + 4
        paths: List[Path] = []
        while next_idx < len(line):
            next_idx, p = Path.from_raw(line, next_idx, rows, cols)
            paths.append(p)

        path_results: List[int] = []
        for p in paths:
            is_valid, valid_length = p.check_validity(all_points_by_coords, rows, cols)
            path_results += [(1 if is_valid else -1), valid_length]

        results.append(" ".join(map(str, path_results)))

    return results
