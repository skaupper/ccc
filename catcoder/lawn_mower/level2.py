from __future__ import annotations

from typing import Iterable, Iterator
from enum import Enum, auto

from ccc_algorithms.geometry_2i import *

class Direction(Enum):
    North = auto()
    East = auto()
    South = auto()
    West = auto()

    @classmethod
    def from_str(cls, value: str) -> Direction:
        match value:
            case "W": return Direction.North
            case "D": return Direction.East
            case "S": return Direction.South
            case "A": return Direction.West
            case _: assert False, f"Unknown direction '{value}'"


    def to_str(self) -> str:
        match self:
            case Direction.North: return "W"
            case Direction.East: return "D"
            case Direction.South: return "S"
            case Direction.West: return "A"

    def to_vec(self) -> Vector2i:
        match self:
            case Direction.North: return Vector2i(0, -1)
            case Direction.East: return Vector2i(1, 0)
            case Direction.South: return Vector2i(0, 1)
            case Direction.West: return Vector2i(-1, 0)


def run(lines: Iterator[str]) -> Iterable[str]:
    l = int(next(lines))

    for _ in range(0, l):
        line = next(lines)
        directions = map(Direction.from_str, line)

        all_points = [Point2i(0,0)]
        for d in directions:
            all_points.append(all_points[-1].move(d.to_vec()))

        path = Path2i(all_points)
        rect = path.get_bounding_box()

        yield f"{rect.get_width()} {rect.get_height()}"
