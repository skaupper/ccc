from __future__ import annotations

from typing import Iterable, Iterator
from enum import Enum, auto

from ccc_algorithms.geometry_2i import *

class Direction(Enum):
    North = auto()
    East = auto()
    South = auto()
    West = auto()

    @classmethod
    def from_str(cls, value: str) -> Direction:
        match value:
            case "W": return Direction.North
            case "D": return Direction.East
            case "S": return Direction.South
            case "A": return Direction.West
            case _: assert False, f"Unknown direction '{value}'"


    def to_str(self) -> str:
        match self:
            case Direction.North: return "W"
            case Direction.East: return "D"
            case Direction.South: return "S"
            case Direction.West: return "A"

    def to_vec(self) -> Vector2i:
        match self:
            case Direction.North: return Vector2i(0, -1)
            case Direction.East: return Vector2i(1, 0)
            case Direction.South: return Vector2i(0, 1)
            case Direction.West: return Vector2i(-1, 0)


def run(lines: Iterator[str]) -> Iterable[str]:
    l = int(next(lines))

    for _ in range(0, l):
        width, height = map(int, next(lines).strip().split(" "))

        # Search for the tree
        tree = None
        for y in range(height):
            row = next(lines)
            for x in range(width):
                if row[x] == "X":
                    tree = Point2i(x,y)
        assert tree is not None

        # Trace the path
        line = next(lines)
        directions = map(Direction.from_str, line)

        all_points = [Point2i(0,0)]
        for d in directions:
            all_points.append(all_points[-1].move(d.to_vec()))

        path = Path2i(all_points)

        # Correct the tree's position, so it is within the same coordinate system as the path
        rect = path.get_bounding_box()
        tree = tree.move(rect.left_top - Point2i(0, 0))

        # Gather all points (path + tree) and check the conditions
        all_points = [tree] + list(path.get_all_points())
        unique_points = set(all_points)

        if len(unique_points) != width*height:
            yield "INVALID"
        elif len(unique_points) != len(all_points):
            yield "INVALID"
        elif rect.get_width() != width or rect.get_height() != height:
            yield "INVALID"
        else:
            yield "VALID"
