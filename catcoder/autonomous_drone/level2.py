from typing import Iterator, Iterable


def run(lines: Iterator[str]) -> Iterable[str]:
    _ = int(next(lines))

    accelerations = [map(int, l.split(" ")) for l in lines]

    velocities: list[list[int]] = []
    for drone in accelerations:
        v = 0
        velocity = []
        for acc in drone:
            v += acc - 10
            velocity.append(v)
        velocities.append(velocity)

    final_height = [sum(d) for d in velocities]

    return [str(h) for h in final_height]
