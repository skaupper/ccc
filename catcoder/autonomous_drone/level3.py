from typing import Iterator, Iterable


def fly_to_the_moon(min_height: int, max_ticks: int) -> list[int]:
    curr_height = 0
    curr_velocity = 0
    acceleration = []
    velocities = []
    heights = []
    ticks = 0

    def apply_accel(acc: int):
        nonlocal curr_height
        nonlocal curr_velocity
        nonlocal acceleration
        nonlocal velocities
        nonlocal heights
        nonlocal ticks

        ticks += 1
        curr_velocity += acc - 10
        curr_height += curr_velocity
        heights.append(curr_height)
        velocities.append(curr_velocity)
        acceleration.append(acc)

    # 1. Reach heights around the min height
    while curr_height < min_height / 2:
        apply_accel(20)

    apply_accel(10)

    while curr_height < min_height or curr_velocity > 0:
        apply_accel(0)
        assert curr_velocity >= 0

    # 2. Increase height by one while not changing the current velocity
    apply_accel(11)
    apply_accel(9)

    # 3. Race to ground
    for acc in reversed(acceleration[:-2]):
        apply_accel(acc)

    # 4. Soft landing
    apply_accel(9)

    print(f"Target height: {min_height}")
    print(f"Heights: {heights}")
    print(f"Vel    : {velocities}")
    print(f"Acc    : {acceleration}")
    print(f"Ticks  : {ticks}")
    print()
    assert ticks <= max_ticks
    return acceleration


def run(lines: Iterator[str]) -> Iterable[str]:
    _ = int(next(lines))
    max_ticks = int(next(lines))
    min_heights = [int(l) for l in lines]
    print(min_heights)

    accelerations: list[list[int]] = []
    for min_height in min_heights:
        accelerations.append(fly_to_the_moon(min_height, max_ticks))

    return [" ".join(map(str, acc)) for acc in accelerations]
