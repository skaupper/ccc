from __future__ import annotations

from typing import Iterator, Iterable
from dataclasses import dataclass


@dataclass(frozen=True)
class Point:
    x: int
    y: int

    def __add__(self, other: Vector) -> Point:
        return Point(self.x + other.x, self.y + other.y)

    def __str__(self) -> str:
        return f"{self.x},{self.y}"

    def __repr__(self) -> str:
        return str(self)


@dataclass(frozen=True)
class Vector:
    x: int
    y: int

    def __str__(self) -> str:
        return f"{self.x},{self.y}"

    def __repr__(self) -> str:
        return str(self)


def sign(n: int) -> int:
    if n > 0:
        return 1
    if n < 0:
        return -1
    return 0


def fly_to_the_moon(landing_pad_x: int, min_height: int, max_ticks: int) -> list[tuple[int, int]]:
    curr_pos = Point(0, 0)
    curr_velocity = Vector(0, 0)
    acceleration = []
    velocities = []
    positions = []
    ticks = 0

    rising_accelerations = []

    def apply_accel(accX: int, accY: int):
        nonlocal curr_pos
        nonlocal curr_velocity
        nonlocal acceleration
        nonlocal velocities
        nonlocal positions
        nonlocal ticks

        assert accY >= 0
        assert 0 <= abs(accX) + abs(accY) <= 20

        ticks += 1
        curr_velocity = Vector(curr_velocity.x + accX, curr_velocity.y + accY - 10)
        curr_pos += curr_velocity
        positions.append(curr_pos)
        velocities.append(curr_velocity)
        acceleration.append(Vector(accX, accY))

    # 2. Increase height by one while not changing the current velocity
    apply_accel(0, 11)
    apply_accel(0, 9)

    # 1. Reach heights around the min height
    while curr_pos.y <= min_height / 2:
        apply_accel(0, 20)

    #######
    apply_accel(0, 10)

    rising_accelerations = acceleration[2:]
    zero_acc_cnt = 0

    # 3. Roughly move to the landing pad
    dir = sign(landing_pad_x - curr_pos.x)
    curr_dir = dir
    distance = curr_dir * (landing_pad_x - curr_pos.x)
    curr_distance = distance

    while abs(landing_pad_x - curr_pos.x) > 10:
        distance = curr_dir * (landing_pad_x - curr_pos.x)
        curr_distance = distance

        while curr_distance > distance / 2:
            acc_y = 0 if (curr_pos.y < min_height or curr_velocity.y > 0) else 10
            if acc_y == 0:
                zero_acc_cnt += 1
            apply_accel(curr_dir * 10, acc_y)
            curr_distance = curr_dir * (landing_pad_x - curr_pos.x)

        while curr_velocity.x != 0:
            acc_y = 0 if (curr_pos.y < min_height or curr_velocity.y > 0) else 10
            if acc_y == 0:
                zero_acc_cnt += 1
            apply_accel(-curr_dir * 10, acc_y)
            curr_distance = curr_dir * (landing_pad_x - curr_pos.x)

        curr_dir *= -1

    # 5. Further adapt Y coord or velocity if needed
    while curr_pos.y < min_height or curr_velocity.y > 0:
        apply_accel(0, 0)
        assert curr_velocity.y >= 0, curr_velocity
        zero_acc_cnt += 1

    rising_accelerations += [Vector(0, 0) for _ in range(zero_acc_cnt)]

    # 4. Get to the landing pad's X coordinate
    distance = landing_pad_x - curr_pos.x
    apply_accel(distance, 10)
    apply_accel(-distance, 10)

    # 5. Race to ground
    for acc in reversed(rising_accelerations):
        apply_accel(0, acc.y)

    # 6. Soft landing
    apply_accel(0, 9)

    print(f"Target height: {min_height}")
    print(f"Target pos   : {landing_pad_x}")
    print(f"Pos   : {positions}")
    print(f"Vel   : {velocities}")
    print(f"Acc   : {acceleration}")
    print(f"Ticks : {ticks}")
    print()

    assert curr_velocity.x == 0
    assert curr_velocity.y in [0, -1]
    assert ticks <= max_ticks
    return acceleration


def run(lines: Iterator[str]) -> Iterable[str]:
    _ = int(next(lines))
    drone_inputs = [map(int, l.split(" ")) for l in lines]

    accelerations: list[list[tuple[int, int]]] = []
    for (landing_pad_x, min_height, max_ticks) in drone_inputs:
        accelerations.append(fly_to_the_moon(landing_pad_x, min_height, max_ticks))

    return [" ".join(map(str, acc)) for acc in accelerations]
