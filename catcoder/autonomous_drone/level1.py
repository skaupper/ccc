from typing import Iterator, Iterable


def run(lines: Iterator[str]) -> Iterable[str]:
    _ = int(next(lines))

    drones = [map(int, l.split(" ")) for l in lines]
    final_height = [sum(d) for d in drones]

    return [str(h) for h in final_height]
