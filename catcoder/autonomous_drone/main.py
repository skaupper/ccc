#!/usr/bin/env python3
import re
import glob
import argparse
from typing import Optional, List, Tuple, Callable, Iterable, Generator, Iterator
from types import ModuleType
from pathlib import Path
from dataclasses import dataclass

# -----------------------------------------------------------------------------
# Add algorithms path
import sys

sys.path.append(str(Path.cwd() / "../.."))
sys.path.append(str(Path.cwd()))

# -----------------------------------------------------------------------------
# Import all level implementations found
import importlib

level_modules: List[ModuleType] = []
while True:
    level_mod = Path.cwd() / f"level{len(level_modules)+1}.py"
    if not level_mod.is_file():
        break

    level_modules.append(
        importlib.import_module(level_mod.with_suffix("").name)  #
    )

# -----------------------------------------------------------------------------


#
# Helper functions
#
def get_inputs(level: int,
               input_dir: Path) -> List[Tuple[Optional[int], Path]]:
    pattern = re.compile(fr"^level{level}_(\d+|example).in$")
    level_inputs = []
    for f in glob.glob(f"{input_dir}/level{level}_*.in"):
        f = Path(f)
        m = pattern.match(f.name)
        if m is None:
            continue

        nr = None
        try:
            nr = int(m.group(1))
        except:
            pass
        level_inputs.append((nr, f))

    level_inputs.sort(key=lambda v: v[0] if v[0] is not None else -1)
    return level_inputs


def get_all_lines(file: Path) -> Generator[str, None, None]:
    with file.open("r") as f:
        l = f.readline()
        while len(l) > 0:
            yield l.strip()
            l = f.readline()


# -----------------------------------------------------------------------------


#
# Entrypoint and main function
#
@dataclass
class CLIArguments:
    example_only: bool
    start_input: int
    end_input: Optional[int]
    level: int
    input_dir: Path
    output_dir: Path


def create_argument_parser() -> argparse.ArgumentParser:
    parser = argparse.ArgumentParser()
    parser.add_argument(
        "-e",
        "--example-only",
        action="store_true",
    )
    parser.add_argument(
        "-i",
        "--input-dir",
        type=Path,
        default=Path(Path.cwd() / "inputs/"),
    )
    parser.add_argument(
        "-o",
        "--output-dir",
        type=Path,
        default=Path(Path.cwd() / "outputs/"),
    )
    parser.add_argument(
        "-l",
        "--level",
        type=int,
        required=True,
    )
    parser.add_argument(
        "--start-input",
        type=int,
        default=1,
    )
    parser.add_argument(
        "--end-input",
        type=int,
        default=None,
    )
    return parser


def main(args: CLIArguments) -> None:

    def exec_level(fn: Callable[[Iterator[str]], Iterable[str]],
                   input_file_path: Path, output_file_path: Path):
        lines = fn(get_all_lines(input_file_path))

        output_file_path.parent.mkdir(parents=True, exist_ok=True)
        with output_file_path.open("w") as f:
            for l in lines:
                f.write(f"{l}\n")

    inputs = get_inputs(args.level, args.input_dir)

    for input_nr, input_file in inputs:
        output_file_path = Path.cwd(
        ) / args.output_dir / input_file.with_suffix(".out").name

        # Extract the run function from the
        assert args.level <= len(
            level_modules
        ), f"Cannot find implementation for level {args.level}"
        assert "run" in dir(
            level_modules[args.level - 1]
        ), f"Cannot find function 'run' in implementation of level {args.level}"
        fn = level_modules[args.level - 1].run

        # Execute example if necessary
        if input_nr is None:
            if args.example_only:
                print(f"Execute example...")
                exec_level(fn, input_file, output_file_path)
            continue
        if args.example_only:
            continue

        # Execute function for a specific input file
        if input_nr >= args.start_input and (args.end_input is None
                                             or input_nr <= args.end_input):
            print(f"Execute input number {input_nr} ...")
            exec_level(fn, input_file, output_file_path)
            continue


if __name__ == "__main__":
    args = CLIArguments(**vars(create_argument_parser().parse_args()))
    main(args)
