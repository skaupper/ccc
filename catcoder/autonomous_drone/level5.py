from __future__ import annotations

from typing import Iterator, Iterable
from dataclasses import dataclass


@dataclass(frozen=True)
class Point:
    x: int
    y: int

    def __add__(self, other: Vector) -> Point:
        return Point(self.x + other.x, self.y + other.y)

    def __str__(self) -> str:
        return f"{self.x},{self.y}"

    def __repr__(self) -> str:
        return str(self)


@dataclass(frozen=True)
class Vector:
    x: int
    y: int

    def __str__(self) -> str:
        return f"{self.x},{self.y}"

    def __repr__(self) -> str:
        return str(self)


@dataclass(frozen=True)
class Building:
    x1: int
    x2: int
    height: int


def sign(n: int) -> int:
    if n > 0:
        return 1
    if n < 0:
        return -1
    return 0


def get_accelerations(distance: int) -> list[int]:
    assert distance >= 0

    MAX_ACCEL = 10

    current_pos = 0
    current_vel = 0
    accelerations = []

    while distance - current_pos != 0:
        remaining_flight_distance = sum(v for v in range(current_vel + MAX_ACCEL, 0, -MAX_ACCEL))

        # 1. Accelerate as fast as possible without shooting beyond the target distance
        if remaining_flight_distance <= distance - current_pos:
            acc = MAX_ACCEL

        # 2. Smooth acceleration so we land exactly at the target distance
        else:
            acc = -MAX_ACCEL
            for try_acc in range(-MAX_ACCEL, MAX_ACCEL):
                remaining_flight_distance_after_braking = sum(v for v in range(current_vel + try_acc, 0, -MAX_ACCEL))
                if remaining_flight_distance_after_braking <= distance - current_pos:
                    acc = try_acc
                else:
                    break

        assert -MAX_ACCEL <= acc <= MAX_ACCEL
        current_vel += acc
        current_pos += current_vel
        assert current_pos <= distance

        accelerations.append(acc)

    assert current_vel <= 10
    if current_vel != 0:
        acc = -current_vel
        current_vel += acc
        current_pos += current_vel
        accelerations.append(acc)

    assert current_vel == 0
    assert current_pos == distance

    return accelerations


def fly_to_the_moon(landing_pad_x: int, landing_pad_y: int, min_height: int, max_ticks: int) -> list[Vector]:
    rising = get_accelerations(min_height)
    rising = [Vector(0, r + 10) for r in rising]

    move = get_accelerations(abs(landing_pad_x))
    move = [Vector(m * sign(landing_pad_x), 10) for m in move]

    falling = get_accelerations(min_height - landing_pad_y - 1)
    falling = [Vector(0, (-f + 10)) for f in falling]

    accelerations = rising + move + falling + [Vector(0, 9)]

    assert max_ticks >= len(accelerations)

    return accelerations


def run(lines: Iterator[str]) -> Iterable[str]:
    nr_of_drones = int(next(lines))
    accelerations: list[list[Vector]] = []

    for _ in range(nr_of_drones):
        landing_pad_x, landing_pad_y, max_ticks = [int(c) for c in next(lines).split(" ")]
        nr_of_buildings = int(next(lines))
        buildings = []

        for _ in range(nr_of_buildings):
            buildings.append(Building(*[int(c) for c in next(lines).split(" ")]))

        if landing_pad_x > 0:
            buildings = [b for b in buildings if b.x1 < landing_pad_x]
        else:
            buildings = [b for b in buildings if b.x2 > landing_pad_x]

        if len(buildings) == 0:
            highest_building = 0
        else:
            highest_building = max(buildings, key=lambda b: b.height).height
        accelerations.append(fly_to_the_moon(landing_pad_x, landing_pad_y, highest_building + 1, max_ticks))

    return [" ".join(map(str, acc)) for acc in accelerations]


if __name__ == "__main__":
    for d in range(2000):
        get_accelerations(d)
