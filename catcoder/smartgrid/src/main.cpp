#include <algorithm>
#include <cassert>
#include <fstream>
#include <iostream>
#include <map>
#include <string>
#include <vector>

// //
// ---------------------------------------------------------------------------------------------------------------------
// // Level 1

// std::vector<double> read_input_1(const std::string &inputFile) {
//     std::ifstream in {inputFile};
//     assert(in);

//     std::vector<double> prices;
//     size_t N;

//     in >> N;

//     for (size_t i = 0; i < N; ++i) {
//         double p;
//         in >> p;
//         prices.push_back(p);
//     }

//     return prices;
// }

// int calc_output_1(const std::vector<double> &prices) {
//     const auto it = std::min_element(prices.cbegin(), prices.cend());
//     return std::distance(prices.cbegin(), it);
// }

// void write_output_1(int value, const std::string &outputFile) {
//     std::cout << value << " " << outputFile << std::endl;
//     std::ofstream out {outputFile};
//     out << value;
// }

// //
// ---------------------------------------------------------------------------------------------------------------------
// // Level 2

// struct task_2_t {
//     int id;
//     int time;
// };

// struct inputs_2_t {
//     std::vector<double> prices;
//     std::vector<task_2_t> tasks;
// };


// inputs_2_t read_input_2(const std::string &inputFile) {
//     std::ifstream in {inputFile};
//     assert(in);


//     inputs_2_t inputs;

//     size_t N;
//     in >> N;
//     for (size_t i = 0; i < N; ++i) {
//         double p;
//         in >> p;
//         inputs.prices.push_back(p);
//     }


//     size_t M;
//     in >> M;
//     for (size_t i = 0; i < M; ++i) {
//         int id;
//         int time;
//         in >> id;
//         in >> time;
//         inputs.tasks.push_back(task_2_t {id, time});
//     }

//     return inputs;
// }


// int find_optimal_start_time(const task_2_t &task, const std::vector<double> &prices) {
//     int lowest_idx = -1;
//     int lowest_value;

//     double current_sum;

//     for (int i = 0; i < task.time; ++i) {
//         current_sum += prices[i];
//     }
//     lowest_value = (int)current_sum;
//     lowest_idx   = 0;

//     for (int i = task.time; i < (int)prices.size(); ++i) {
//         current_sum = current_sum + prices[i] - prices[i - task.time];
//         if ((int)current_sum < lowest_value) {
//             lowest_idx   = (i - task.time) + 1;
//             lowest_value = current_sum;
//         }
//     }

//     return lowest_idx;
// }

// std::vector<std::pair<int, int>> calc_output_2(const inputs_2_t &inputs) {
//     std::vector<std::pair<int, int>> optimal_start_times;

//     for (int i = 0; i < (int)inputs.tasks.size(); ++i) {
//         optimal_start_times.emplace_back(inputs.tasks[i].id, find_optimal_start_time(inputs.tasks[i],
//         inputs.prices));
//     }

//     return optimal_start_times;
// }

// void write_output_2(const std::vector<std::pair<int, int>> &value, const std::string &outputFile) {
//     std::ofstream out {outputFile};

//     out << value.size() << std::endl;
//     for (const auto &e : value) {
//         out << e.first << " " << e.second << std::endl;
//     }
// }


// //
// ---------------------------------------------------------------------------------------------------------------------
// // Level 3

// struct task_3_t {
//     int id;
//     int power;
//     int start_time;
//     int end_time;
// };

// struct inputs_3_t {
//     std::vector<double> prices;
//     std::vector<task_3_t> tasks;
// };


// inputs_3_t read_input_3(const std::string &inputFile) {
//     std::ifstream in {inputFile};
//     assert(in);


//     inputs_3_t inputs;

//     size_t N;
//     in >> N;
//     for (size_t i = 0; i < N; ++i) {
//         double p;
//         in >> p;
//         inputs.prices.push_back(p);
//     }


//     size_t M;
//     in >> M;
//     for (size_t i = 0; i < M; ++i) {
//         int id, power, start, end;
//         in >> id >> power >> start >> end;
//         inputs.tasks.push_back(task_3_t {id, power, start, end});
//     }

//     return inputs;
// }


// struct outputs_3_t {
//     int tid;
//     int mid;
//     int draw;
// };


// int find_optimal_minute(const task_3_t &task, const std::vector<double> &prices) {
//     int min_value = prices[task.start_time];
//     int min_idx   = task.start_time;
//     for (int i = task.start_time; i <= task.end_time; ++i) {
//         if (prices[i] < min_value) {
//             min_value = prices[i];
//             min_idx   = i;
//         }
//     }
//     return min_idx;
// }

// std::vector<outputs_3_t> calc_output_3(const inputs_3_t &inputs) {
//     std::vector<outputs_3_t> outputs;

//     for (int i = 0; i < (int)inputs.tasks.size(); ++i) {
//         outputs.emplace_back(outputs_3_t {inputs.tasks[i].id, find_optimal_minute(inputs.tasks[i], inputs.prices),
//                                           inputs.tasks[i].power});
//     }

//     return outputs;
// }

// void write_output_3(const std::vector<outputs_3_t> &value, const std::string &outputFile) {
//     std::ofstream out {outputFile};

//     out << value.size() << std::endl;
//     for (const auto &e : value) {
//         out << e.tid << " " << e.mid << " " << e.draw << std::endl;
//     }
// }


// //
// ---------------------------------------------------------------------------------------------------------------------
// // Level 4

// struct task_4_t {
//     int id;
//     int power;
//     int start_time;
//     int end_time;

//     int total_power = 0;
//     std::map<int, int> power_drawn_at_minute {};
// };

// struct inputs_4_t {
//     int max_price;
//     int max_power;

//     std::vector<double> prices;
//     std::vector<int> power_drawn;
//     std::vector<task_4_t> tasks;
// };


// inputs_4_t read_input_4(const std::string &inputFile) {
//     std::ifstream in {inputFile};
//     assert(in);


//     inputs_4_t inputs;

//     in >> inputs.max_power >> inputs.max_price;

//     size_t N;
//     in >> N;
//     for (size_t i = 0; i < N; ++i) {
//         double p;
//         in >> p;
//         inputs.prices.push_back(p);
//         inputs.power_drawn.push_back(0);
//     }


//     size_t M;
//     in >> M;
//     for (size_t i = 0; i < M; ++i) {
//         int id, power, start, end;
//         in >> id >> power >> start >> end;
//         inputs.tasks.push_back(task_4_t {id, power, start, end});
//     }

//     return inputs;
// }


// struct draw_4_t {
//     int mid;
//     int draw;
// };

// struct outputs_4_t {
//     int tid;
//     std::vector<draw_4_t> draws;
// };


// void do_iteration(task_4_t &task, inputs_4_t &inputs, const size_t iterations) {
//     // Whats the cheapest minute in the given time range which still has power left to be drawn
//     int min_value = -1;
//     int min_idx   = -1;
//     for (int i = task.start_time; i <= task.end_time; ++i) {
//         if ((min_idx == -1 || inputs.prices[i] < min_value) && inputs.power_drawn[i] < inputs.max_power) {
//             min_value = inputs.prices[i];
//             min_idx   = i;
//         }
//     }

//     // The actual draw is determined by:
//     // 1.) How much power has the task left to be drawn
//     // 2.) How much more power can be drawn this minute
//     // 3.) A maximum draw of power per iteration
//     int max_draw = std::min({task.power - task.total_power, inputs.max_power - inputs.power_drawn[min_idx]});
//     int draw     = task.power / iterations;
//     if (draw == 0) {  // avoid 0 draws
//         draw = 1;
//     }

//     draw = std::min({max_draw, draw});
//     assert(draw != 0);

//     task.total_power += draw;
//     task.power_drawn_at_minute[min_idx] += draw;
//     inputs.power_drawn[min_idx] += draw;
// }

// bool has_finished(const inputs_4_t &inputs) {
//     for (const auto &t : inputs.tasks) {
//         if (t.total_power < t.power) {
//             return false;
//         }
//     }

//     // Iterations finished. Check power and price constraints...
//     int bill = 0;
//     for (int i = 0; i < (int)inputs.power_drawn.size(); ++i) {
//         if (inputs.power_drawn[i] > inputs.max_power) {
//             std::cerr << "Max power exceeded at minute " << i << "!" << std::endl;
//         }

//         bill += inputs.power_drawn[i] * inputs.prices[i];
//     }
//     if (bill > inputs.max_price) {
//         std::cerr << "Max price of " << inputs.max_price << " exceeded (" << bill << ")!" << std::endl;
//     }
//     return true;
// }

// std::vector<outputs_4_t> calc_output_4(inputs_4_t &inputs) {
//     // Increase the power draw for all tasks step by step to avoid bad results
//     constexpr int ITERATIONS = 1;
//     while (!has_finished(inputs)) {
//         for (auto &t : inputs.tasks) {
//             if (t.power == t.total_power) {
//                 continue;
//             }
//             do_iteration(t, inputs, ITERATIONS);
//         }
//     }


//     // Generate outputs
//     std::vector<outputs_4_t> outputs;
//     for (auto &t : inputs.tasks) {
//         outputs_4_t o;
//         o.tid = t.id;
//         for (const auto &kv : t.power_drawn_at_minute) {
//             if (kv.second == 0) {
//                 continue;
//             }
//             o.draws.push_back(draw_4_t {kv.first, kv.second});
//         }
//         outputs.push_back(o);
//     }
//     return outputs;
// }

// void write_output_4(const std::vector<outputs_4_t> &value, const std::string &outputFile) {
//     std::ofstream out {outputFile};

//     out << value.size() << std::endl;
//     for (const auto &e : value) {
//         out << e.tid << " ";
//         for (const auto &d : e.draws) {
//             out << d.mid << " " << d.draw << " ";
//         }
//         out << std::endl;
//     }
// }


// ---------------------------------------------------------------------------------------------------------------------
// Level 5

struct task_5_t {
    int id;
    int power;
    int start_time;
    int end_time;

    int total_power = 0;
    std::map<int, int> power_drawn_at_minute {};
};

struct inputs_5_t {
    int max_price;
    int max_power;
    int max_concurrent;

    std::vector<double> prices;
    std::vector<int> power_drawn;
    std::vector<task_5_t> tasks;
};


inputs_5_t read_input_5(const std::string &inputFile) {
    std::ifstream in {inputFile};
    assert(in);


    inputs_5_t inputs;

    in >> inputs.max_power >> inputs.max_price >> inputs.max_concurrent;

    size_t N;
    in >> N;
    for (size_t i = 0; i < N; ++i) {
        double p;
        in >> p;
        inputs.prices.push_back(p);
        inputs.power_drawn.push_back(0);
    }


    size_t M;
    in >> M;
    for (size_t i = 0; i < M; ++i) {
        int id, power, start, end;
        in >> id >> power >> start >> end;
        inputs.tasks.push_back(task_5_t {id, power, start, end});
    }

    return inputs;
}


struct draw_5_t {
    int mid;
    int draw;
};

struct outputs_5_t {
    int tid;
    std::vector<draw_5_t> draws;
};


void do_iteration(task_5_t &task, inputs_5_t &inputs, const size_t iterations) {
    // Whats the cheapest minute in the given time range which still has power left to be drawn
    int min_value = -1;
    int min_idx   = -1;
    for (int i = task.start_time; i <= task.end_time; ++i) {
        if ((min_idx == -1 || inputs.prices[i] < min_value) && inputs.power_drawn[i] < inputs.max_power) {
            min_value = inputs.prices[i];
            min_idx   = i;
        }
    }

    // The actual draw is determined by:
    // 1.) How much power has the task left to be drawn
    // 2.) How much more power can be drawn this minute
    // 3.) A maximum draw of power per iteration
    int max_draw = std::min({task.power - task.total_power, inputs.max_power - inputs.power_drawn[min_idx]});
    int draw     = task.power / iterations;
    if (draw == 0) {  // avoid 0 draws
        draw = 1;
    }

    draw = std::min({max_draw, draw});
    assert(draw != 0);

    task.total_power += draw;
    task.power_drawn_at_minute[min_idx] += draw;
    inputs.power_drawn[min_idx] += draw;
}

bool has_finished(const inputs_5_t &inputs) {
    for (const auto &t : inputs.tasks) {
        if (t.total_power < t.power) {
            return false;
        }
    }

    // Iterations finished. Check power and price constraints...
    int bill = 0;
    for (int i = 0; i < (int)inputs.power_drawn.size(); ++i) {
        if (inputs.power_drawn[i] > inputs.max_power) {
            std::cerr << "Max power exceeded at minute " << i << "!" << std::endl;
        }

        bill += inputs.power_drawn[i] * inputs.prices[i];
    }
    if (bill > inputs.max_price) {
        std::cerr << "Max price of " << inputs.max_price << " exceeded (" << bill << ")!" << std::endl;
    }

    for (int i = 0; i < (int)inputs.prices.size(); ++i) {
        int total_tasks = 0;
        for (const auto &t : inputs.tasks) {
            if (t.power_drawn_at_minute.find(i) != t.power_drawn_at_minute.cend()
                && t.power_drawn_at_minute.at(i) != 0) {
                total_tasks++;
            }
        }

        if (total_tasks > inputs.max_concurrent) {
            std::cerr << "Max concurrency for minute " << i << " exceeded (" << total_tasks << ")!" << std::endl;
        }
    }
    return true;
}

std::vector<outputs_5_t> calc_output_5(inputs_5_t &inputs) {
    // Increase the power draw for all tasks step by step to avoid bad results
    constexpr int ITERATIONS = 1;
    while (!has_finished(inputs)) {
        for (auto &t : inputs.tasks) {
            if (t.power == t.total_power) {
                continue;
            }
            do_iteration(t, inputs, ITERATIONS);
        }
    }


    // Generate outputs
    std::vector<outputs_5_t> outputs;
    for (auto &t : inputs.tasks) {
        outputs_5_t o;
        o.tid = t.id;
        for (const auto &kv : t.power_drawn_at_minute) {
            if (kv.second == 0) {
                continue;
            }
            o.draws.push_back(draw_5_t {kv.first, kv.second});
        }
        outputs.push_back(o);
    }
    return outputs;
}

void write_output_5(const std::vector<outputs_5_t> &value, const std::string &outputFile) {
    std::ofstream out {outputFile};

    out << value.size() << std::endl;
    for (const auto &e : value) {
        out << e.tid << " ";
        for (const auto &d : e.draws) {
            out << d.mid << " " << d.draw << " ";
        }
        out << std::endl;
    }
}


int main() {
    constexpr int STAGE        = 5;
    constexpr int TOTAL_LEVELS = 5;


    std::cout << "Example input..." << std::endl;
    auto in        = read_input_5(std::string {"inputs/level"} + std::to_string(STAGE) + "_example.in");
    const auto out = calc_output_5(in);
    write_output_5(out, std::string {"outputs/level"} + std::to_string(STAGE) + "_example.out");

    (void)getc(stdin);

    for (int i = 1; i <= TOTAL_LEVELS; ++i) {
        std::cout << "Level " << i << "..." << std::endl;
        std::string filename_in {std::string {"inputs/level"} + std::to_string(STAGE) + "_" + std::to_string(i)
                                 + ".in"};
        std::string filename_out {std::string {"outputs/level"} + std::to_string(STAGE) + "_" + std::to_string(i)
                                  + ".out"};

        auto in        = read_input_5(filename_in);
        const auto out = calc_output_5(in);
        write_output_5(out, filename_out);
    }

    return 0;
}
